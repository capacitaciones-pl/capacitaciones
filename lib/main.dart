import 'package:app_capacitaciones/features/capacitacion/presentation/pages/capacitacion.dart';
import 'package:app_capacitaciones/features/capacitacion/presentation/pages/material.dart' as mp;
import 'package:app_capacitaciones/features/capacitaciones/presentation/pages/capacitaciones.dart';
import 'package:app_capacitaciones/features/capacitaciones_programadas/presentation/pages/capacitaciones_programadas.dart';
import 'package:app_capacitaciones/features/evaluacion/presentation/pages/evaluacion.dart';
import 'package:app_capacitaciones/features/mis_capacitaciones/presentation/pages/mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/proximas_capacitaciones/presentation/pages/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/landing/presentation/pages/landing.dart';
import 'package:app_capacitaciones/features/login/presentation/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:app_capacitaciones/injection_container.dart' as di;
import 'package:flutter/services.dart';

import 'features/capacitacion_inscripcion/presentation/pages/capacitacion_inscripcion.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'HSEC Capacitaciones',
      theme: ThemeData(
        primaryColor: Colors.white,
        //visualDensity: VisualDensity.,
        fontFamily: 'Ubuntu',
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        Routes[0].route: (context) => LandingPage(),
        Routes[1].route: (context) => MisCapacitacionesPage(),
        Routes[2].route: (context) => CapacitacionesProgramadasPage(),
        Routes[3].route: (context) => ProximasCapacitacionesPage(),
        '/capacitacion': (context) => CapacitacionPage(),
        '/evaluacion': (context) => EvaluacionPage(),
        '/capacitaciones': (context) => CapacitacionesPage(),
        '/capacitacion_ins': (context) => CapacitacionInscripcionPage(),
        '/material': (context) => mp.MaterialPage(),
      },
    );
  }
}