const String MHSEC_WEB = 'https://app.antapaccay.com.pe/mhsec_web';
const String MHSEC_GEN = '$MHSEC_WEB/mhsec_gen/api';
const String MHSEC_CAP = '$MHSEC_WEB/mhsec_cap/api';
const String MHSEC_PLAN = '$MHSEC_WEB/mhsec_plan/api';