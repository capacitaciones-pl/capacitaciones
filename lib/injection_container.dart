import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';
import 'package:app_capacitaciones/features/capacitacion/data/datasources/materiales_curso_data_source.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/repositories/material_curso_repository.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/usecases/actualizar_material.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/usecases/cargar_material.dart';
import 'package:app_capacitaciones/features/capacitacion/presentation/bloc/capacitacion_bloc.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/repositories/capacitacion_inscripcion_repository.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/usecases/desinscribir_capacitacion.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/usecases/inscribir_capacitacion.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/presentation/bloc/capacitacion_inscripcion_bloc.dart';
import 'package:app_capacitaciones/features/capacitaciones/presentation/bloc/capacitaciones_bloc.dart';
import 'package:app_capacitaciones/features/capacitaciones_programadas/presentation/bloc/capacitaciones_programadas_bloc.dart';
import 'package:app_capacitaciones/features/evaluacion/data/repositories/evaluacion_repository_impl.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/repositories/evaluacion_repository.dart';
import 'package:app_capacitaciones/features/landing/data/datasources/capacitaciones_vencer_data_source.dart';
import 'package:app_capacitaciones/features/landing/data/repositories/capacitaciones_vencer_repository_impl.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/capacitaciones_vencer_repository.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/mis_capacitaciones_repository.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/proximas_capacitaciones_repository.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/presentation/bloc/landing_bloc.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_remote_data_source.dart';
import 'package:app_capacitaciones/features/login/data/repositories/user_data_repository_impl.dart';
import 'package:app_capacitaciones/features/login/domain/repositories/user_data_repository.dart';
import 'package:app_capacitaciones/features/login/domain/usecases/get_user_data.dart';
import 'package:app_capacitaciones/features/login/presentation/bloc/user_data_bloc.dart';
import 'package:app_capacitaciones/features/proximas_capacitaciones/presentation/bloc/proximas_capacitaciones_bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'features/capacitacion/data/repositories/material_curso_repository_impl.dart';
import 'features/capacitacion_inscripcion/data/datasources/capacitacion_inscripcion_data_source.dart';
import 'features/capacitacion_inscripcion/data/repositories/capacitacion_inscripcion_repository_impl.dart';
import 'features/capacitaciones/data/datasources/capacitaciones_disponibles_data_source.dart';
import 'features/capacitaciones/data/repositories/capacitaciones_disponibles_repository_impl.dart';
import 'features/capacitaciones/domain/repositories/capacitaciones_disponibles_repository.dart';
import 'features/capacitaciones/domain/usecases/get_capacitaciones_disponibles.dart';
import 'features/landing/data/datasources/mis_capacitaciones_data_source.dart';
import 'features/landing/data/datasources/proximas_capacitaciones_data_source.dart';
import 'features/landing/data/repositories/mis_capacitaciones_repository_impl.dart';
import 'features/landing/data/repositories/proximas_capacitaciones_repository_impl.dart';
import 'features/mis_capacitaciones/presentation/bloc/mis_capacitaciones_bloc.dart';
import 'features/evaluacion/presentation/bloc/evaluacion_bloc.dart';
import 'features/evaluacion/domain/usecases/get_preguntas.dart';
import 'features/evaluacion/domain/repositories/preguntas_curso_repository.dart';
import 'features/evaluacion/data/repositories/preguntas_curso_repository_impl.dart';
import 'features/evaluacion/data/datasources/evaluacion_data_source.dart';
import 'features/evaluacion/domain/usecases/send_respuestas.dart';

final sL = GetIt.instance;

Future<void> init() async {
  //! Features - Login
  // Bloc
  sL.registerFactory(() => UserDataBloc(getUserDataCase: sL()));
  // Use cases
  sL.registerLazySingleton(() => GetUserData(sL()));
  // Repository
  sL.registerLazySingleton<UserDataRepository>(
    () => UserDataRepositoryImpl(
      localDataSource: sL(),
      remoteDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  // Data
  sL.registerLazySingleton<UserDataLocalDataSource>(
    () => UserDataLocalDataSourceImpl(
      sharedPreferences: sL(),
    ),
  );
  sL.registerLazySingleton<UserDataRemoteDataSource>(
    () => UserDataRemoteDataSourceImpl(
      client: sL(),
    ),
  );

  //! Features - Landing
  // Bloc
  sL.registerFactory(
    () => LandingBloc(
      getCapacionesPorVencerCase: sL(),
      getMisCapacitacionesCase: sL(),
      getProximasCapacitacionesCase: sL(),
    ),
  );
  // Use cases
  sL.registerLazySingleton(() => GetCapacionesPorVencer(sL()));
  sL.registerLazySingleton(() => GetMisCapacitaciones(sL()));
  sL.registerLazySingleton(() => GetProximasCapacitaciones(sL()));
  // Repositories
  sL.registerLazySingleton<CapacitacionesPorVencerRepository>(
    () => CapacitacionesPorVencerRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  sL.registerLazySingleton<MisCapacitacionesRepository>(
    () => MisCapacitacionesRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  sL.registerLazySingleton<ProximasCapacitacionesRepository>(
    () => ProximasCapacitacionesRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  // Data
  sL.registerLazySingleton<CapacitacionesPorVencerDataSource>(
    () => CapacitacionesPorVencerDataSourceImpl(
      client: sL(),
    ),
  );
  sL.registerLazySingleton<MisCapacitacionesDataSource>(
        () => MisCapacitacionesDataSourceImpl(
      client: sL(),
    ),
  );
  sL.registerLazySingleton<ProximasCapacitacionesDataSource>(
        () => ProximasCapacitacionesDataSourceImpl(
      client: sL(),
    ),
  );

  //! Features - Capacitaciones Disponibles
  // Bloc
  sL.registerFactory(
    () => CapacitacionesBloc(
      getCapacitacionesDisponiblesCase: sL(),
    ),
  );
  // Use cases
  sL.registerLazySingleton(() => GetCapacitacionesDisponibles(sL()));
  // Repositories
  sL.registerLazySingleton<CapacitacionesDisponiblesRepository>(
        () => CapacitacionesDisponiblesRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  // Data
  sL.registerLazySingleton<CapacitacionesDisponiblesDataSource>(
        () => CapacitacionesDisponiblesDataSourceImpl(
      client: sL(),
    ),
  );

  //! Features - Capacitacion Inscripcion
  // Bloc
  sL.registerFactory(
    () => CapacitacionInscripcionBloc(
      inscribirCapacitacionCase: sL(),
      desinscribirCapacitacionCase: sL(),
    ),
  );
  // Uses cases
  sL.registerLazySingleton(() => DesinscribirCapacitacion(sL()));
  sL.registerLazySingleton(() => InscribirCapacitacion(sL()));
  // Repositories
  sL.registerLazySingleton<CapacitacionInscripcionRepository>(
        () => CapacitacionInscripcionRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  // Data
  sL.registerLazySingleton<CapacitacionInscripcionDataSource>(
    () => CapacitacionInscripcionDataSourceImpl(
      client: sL(),
    ),
  );

  //! Features - Mis capacitaciones
  // Bloc
  sL.registerFactory(
    () => MisCapacitacionesBloc(
      getMisCapacitacionesCase: sL(),
    ),
  );

  //! Features - Mis capacitaciones programadas
  // Bloc
  sL.registerFactory(
    () => CapacitacionesProgramadasBloc(
      getProximasCapacitacionesCase: sL(),
    ),
  );

  //! Features - Proximas capacitaciones
  // Bloc
  sL.registerFactory(
    () => ProximasCapacitacionesBloc(
      getCapacionesPorVencerCase: sL(),
    ),
  );

  //! Features - Capacitacion
  // Bloc
  sL.registerFactory(
    () => CapacitacionBloc(
      cargarMaterialCase: sL(),
      actualizarMaterialCase: sL(),
    ),
  );
  // Uses cases
  sL.registerLazySingleton(() => CargarMaterial(sL()));
  sL.registerLazySingleton(() => ActualizarMaterial(sL()));
  // Repositories
  sL.registerLazySingleton<MaterialCursoRepository>(
        () => MaterialCursoRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  // Data
  sL.registerLazySingleton<MaterialesCursoDataSource>(
        () => MaterialesCursoDataSourceImpl(
      client: sL(),
    ),
  );

  //! Features - Evaluacion
  // Bloc
  sL.registerFactory(
    () => EvaluacionBloc(
      getPreguntasCase: sL(),
      sendRespuestasCase: sL(),
    ),
  );
  // Uses cases
  sL.registerLazySingleton(() => GetPreguntas(sL()));
  sL.registerLazySingleton(() => SendRespuestas(sL()));
  // Repositories
  sL.registerLazySingleton<PreguntasCursoRepository>(
        () => PreguntasCursoRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  sL.registerLazySingleton<EvaluacionRepository>(
        () => EvaluacionRepositoryImpl(
      remoteDataSource: sL(),
      userDataLocalDataSource: sL(),
      networkInfo: sL(),
    ),
  );
  // Data
  sL.registerLazySingleton<EvaluacionDataSource>(
        () => EvaluacionDataSourceImpl(
      client: sL(),
    ),
  );

  //! Core
  sL.registerLazySingleton(() => InputConverter());
  sL.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sL()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sL.registerLazySingleton(() => sharedPreferences);
  sL.registerLazySingleton(() => http.Client());
  sL.registerLazySingleton(() => DataConnectionChecker());
}

void initFeatures() {

}