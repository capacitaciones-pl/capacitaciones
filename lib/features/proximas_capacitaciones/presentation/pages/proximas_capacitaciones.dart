import 'dart:convert';

import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/proximas_capacitaciones/presentation/bloc/proximas_capacitaciones_bloc.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:app_capacitaciones/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';

class ProximasCapacitacionesPage extends StatefulWidget {
  @override
  _ProximasCapacitacionesPageState createState() => _ProximasCapacitacionesPageState();
}

class _ProximasCapacitacionesPageState extends State<ProximasCapacitacionesPage> {
  String name;

  @override
  void initState() {
    super.initState();
    SharedPreferences sharedPreferences = sL<SharedPreferences>();
    final jsonString = sharedPreferences.getString('CACHED_USER_DATA');
    UserDataModel userData = UserDataModel.fromJson(json.decode(jsonString));
    name = userData.nombres.split(' ')[0].capitalize() +
        " " +
        userData.apellidos.split(' ')[0].capitalize();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sL<ProximasCapacitacionesBloc>()..add(GetProximasCapacitacionesEvent()),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Próximas Capacitaciones", true),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(
          index: 3,
          name: name,
        ),
        body: BlocBuilder<ProximasCapacitacionesBloc, ProximasCapacitacionesState>(
          builder: (context, state) {
            if (state is Loading) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is Loaded) {
              CapacitacionesPorVencer capacitacionesPorVencer = state.capacitacionesPorVencer;
              return buildList(capacitacionesPorVencer);
            } else {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Widget buildList(CapacitacionesPorVencer capacitacionesPorVencer) {
    var capacitaciones = capacitacionesPorVencer.data;

    return ListView.separated(
      padding: EdgeInsets.all(16),
      itemCount: capacitaciones.length,
      itemBuilder: (BuildContext context, int index) {
        return Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                '/capacitacion_ins',
                arguments: capacitaciones[index],
              );
            },
            child: Container(
              width: 200.0,
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 0.5,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        capacitaciones[index].enlace != null && capacitaciones[index].enlace.length > 0 ?
                        Icons.ondemand_video :
                        Icons.collections_bookmark,
                        color: Color.fromRGBO(209, 77, 56, 1.0),
                        size: 26,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Text(
                          capacitaciones[index].tema,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14 / myTextScaleFactor,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(85, 85, 85, 1.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                          'Empresa capacitadora',
                          style: TextStyle(
                            fontSize: 13 / myTextScaleFactor,
                            fontWeight: FontWeight.w300,
                            color: Color.fromRGBO(85, 85, 85, 1.0),
                          ),
                        ),

                      SizedBox(
                        width: 6,
                      ),
                      Expanded(
                        child: Text(
                          capacitaciones[index].empresaCap,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 13 / myTextScaleFactor,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(85, 85, 85, 1.0),
                          ),
                        ),
                      ),

                    ],
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "Fecha de capacitación",
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text(
                        DateFormat('dd/MM/yyyy').format(capacitaciones[index].fechaIni),
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 16,
        );
      },
    );
  }
}