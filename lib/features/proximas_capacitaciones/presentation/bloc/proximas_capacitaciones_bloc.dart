import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_capacitaciones_vencer.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'proximas_capacitaciones_event.dart';
part 'proximas_capacitaciones_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class ProximasCapacitacionesBloc extends Bloc<ProximasCapacitacionesEvent, ProximasCapacitacionesState> {
  final GetCapacionesPorVencer getCapacionesPorVencer;

  ProximasCapacitacionesBloc({
    @required GetCapacionesPorVencer getCapacionesPorVencerCase,
  })  : assert(getCapacionesPorVencerCase != null),
        getCapacionesPorVencer = getCapacionesPorVencerCase,
        super(Empty());

  @override
  Stream<ProximasCapacitacionesState> mapEventToState(
    ProximasCapacitacionesEvent event,
  ) async* {
    if (event is GetProximasCapacitacionesEvent) {
      yield Loading();

      final failureOrCapacitacionesPorVencer =
          await getCapacionesPorVencer(NoParams());

      yield* failureOrCapacitacionesPorVencer.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (data) async* {
          yield Loaded(
            capacitacionesPorVencer: data,
          );
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
