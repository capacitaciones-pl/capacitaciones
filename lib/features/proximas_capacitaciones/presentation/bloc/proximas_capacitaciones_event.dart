part of 'proximas_capacitaciones_bloc.dart';

abstract class ProximasCapacitacionesEvent extends Equatable {
  const ProximasCapacitacionesEvent();
}

class GetProximasCapacitacionesEvent extends ProximasCapacitacionesEvent {
  @override
  List<Object> get props => [];
}
