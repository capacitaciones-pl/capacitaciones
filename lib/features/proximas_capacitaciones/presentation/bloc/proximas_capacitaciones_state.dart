part of 'proximas_capacitaciones_bloc.dart';

abstract class ProximasCapacitacionesState extends Equatable {
  const ProximasCapacitacionesState();
}

class Empty extends ProximasCapacitacionesState {
  @override
  List<Object> get props => [];
}

class Loading extends ProximasCapacitacionesState {
  @override
  List<Object> get props => [];
}

class Loaded extends ProximasCapacitacionesState {
  final CapacitacionesPorVencer capacitacionesPorVencer;

  Loaded({
    @required this.capacitacionesPorVencer,
  });

  @override
  List<Object> get props => [
    capacitacionesPorVencer,
  ];
}

class Error extends ProximasCapacitacionesState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
