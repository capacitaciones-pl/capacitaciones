import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/capacitaciones/data/datasources/capacitaciones_disponibles_data_source.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/repositories/capacitaciones_disponibles_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class CapacitacionesDisponiblesRepositoryImpl
    implements CapacitacionesDisponiblesRepository {

  final CapacitacionesDisponiblesDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  CapacitacionesDisponiblesRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, CapacitacionesDisponibles>>
      getCapacitacionesDisponibles(String codCurso) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final remoteUserData = await remoteDataSource.getCapacitacionesDisponibles(
            localUserData.codPersona, codCurso, localUserData.token);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}