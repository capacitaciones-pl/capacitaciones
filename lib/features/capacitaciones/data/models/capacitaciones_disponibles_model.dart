import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:meta/meta.dart';

class CapacitacionesDisponiblesModel extends CapacitacionesDisponibles {
  CapacitacionesDisponiblesModel({
    @required int count,
    @required List<CapacitacionDisponible> data,
  }) : super(
    count: count,
    data: data,
  );

  factory CapacitacionesDisponiblesModel.fromJson(Map<String, dynamic> json) {
    return CapacitacionesDisponiblesModel(
      count: json['count'],
      data: (json['data'] as List).map((e) => CapacitacionDisponibleModel.fromJson(e)).toList(),
    );
  }
}

class CapacitacionDisponibleModel extends CapacitacionDisponible {
  CapacitacionDisponibleModel({
    @required String codCurso,
    @required String tema,
    @required DateTime fechaIni,
    @required DateTime fechaFinal,
    @required DateTime fechaVen,
    @required int vigencia,
    @required String codVigenciaCapacita,
    @required String duracion,
    @required String horaInicio,
    @required String tipo,
    @required double puntajeTotal,
    @required int capacidad,
    @required String codEmpresaCap,
    @required String codParticipante,
    @required String codTemaCapacita,
    @required bool inscrito,
    @required int disponibles,
    @required List<Expositor> expositores,
  }) : super(
    codCurso: codCurso,
    tema: tema,
    fechaIni: fechaIni,
    fechaFinal: fechaFinal,
    fechaVen: fechaVen,
    vigencia: vigencia,
    codVigenciaCapacita: codVigenciaCapacita,
    duracion: duracion,
    horaInicio: horaInicio,
    tipo: tipo,
    puntajeTotal: puntajeTotal,
    capacidad: capacidad,
    codEmpresaCap: codEmpresaCap,
    codParticipante: codParticipante,
    codTemaCapacita: codTemaCapacita,
    inscrito: inscrito,
    disponibles: disponibles,
    expositores: expositores,
  );

  factory CapacitacionDisponibleModel.fromJson(Map<String, dynamic> json) {
    return CapacitacionDisponibleModel(
      codCurso: json['codCurso'],
      tema: json['tema'],
      fechaIni: json['fechaIni'] == null ? null : DateTime.parse(json['fechaIni']),
      fechaFinal: json['fechaFinal'] == null ? null : DateTime.parse(json['fechaFinal']),
      fechaVen: json['fechaVen'] == null ? null : DateTime.parse(json['fechaVen']),
      vigencia: json['vigencia'],
      codVigenciaCapacita: json['codVigenciaCapacita'],
      duracion: json['duracion'],
      horaInicio: json['horaInicio'],
      tipo: json['tipo'],
      puntajeTotal: json['puntajeTotal'],
      capacidad: json['capacidad'],
      codEmpresaCap: json['codEmpresaCap'],
      codParticipante: json['codParticipante'],
      codTemaCapacita: json['codTemaCapacita'],
      inscrito: json['inscrito'],
      disponibles: json['disponibles'],
      expositores: (json['expositores'] as List).map((e) => ExpositorModel.fromJson(e)).toList(),
    );
  }
}

class ExpositorModel extends Expositor {
  ExpositorModel({
    @required String codPersona,
    @required String codTemaCapacita,
    @required bool tipo,
    @required String nombre,
  }) : super(
    codPersona : codPersona,
    codTemaCapacita: codTemaCapacita,
    tipo: tipo,
    nombre: nombre,
  );

  factory ExpositorModel.fromJson(Map<String, dynamic> json) {
    return ExpositorModel(
      codPersona: json['codPersona'],
      codTemaCapacita: json['codTemaCapacita'],
      tipo: json['tipo'],
      nombre: json['nombre'],
    );
  }
}