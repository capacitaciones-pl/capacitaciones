import 'dart:convert';

import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/capacitaciones/data/models/capacitaciones_disponibles_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class CapacitacionesDisponiblesDataSource {
  Future<CapacitacionesDisponiblesModel> getCapacitacionesDisponibles(String codPersona, String codCurso, String token);
}

class CapacitacionesDisponiblesDataSourceImpl implements CapacitacionesDisponiblesDataSource {
  final http.Client client;

  CapacitacionesDisponiblesDataSourceImpl({
    @required this.client,
  });

  @override
  Future<CapacitacionesDisponiblesModel> getCapacitacionesDisponibles(String codPersona, String codCurso, String token) async {
    final response = await client.get(
      'https://app.antapaccay.com.pe/proportaltest/mhsec_cap/api/v1/capacitaciones/getmiscursosdisponibles/getestadocursos/$codPersona/$codCurso',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return CapacitacionesDisponiblesModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}