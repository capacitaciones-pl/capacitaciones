import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:app_capacitaciones/features/capacitaciones/presentation/bloc/capacitaciones_bloc.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../injection_container.dart';

class CapacitacionesPage extends StatefulWidget {
  @override
  _CapacitacionesPageState createState() => _CapacitacionesPageState();
}

class _CapacitacionesPageState extends State<CapacitacionesPage> {
  String codCurso;

  @override
  Widget build(BuildContext context) {
    codCurso = ModalRoute.of(context).settings.arguments;
    return BlocProvider(
      create: (context) => sL<CapacitacionesBloc>()
        ..add(GetCapacitacionesDataEvent(codCurso)),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Capacitaciones Disponibles", false),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(index: 1,),
        body: buildCapacitacionesDisponibles(),
      ),
    );
  }

  Widget buildCapacitacionesDisponibles() {
    return BlocBuilder<CapacitacionesBloc, CapacitacionesState>(
      builder: (context, state) {
        if (state is Loading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is Loaded) {
          final List<CapacitacionDisponible> capacitacionesDisponibles =
              state.capacitacionesDisponibles.data;
          return ListView.separated(
            padding: EdgeInsets.all(16),
            itemCount: state.capacitacionesDisponibles.count,
            itemBuilder: (BuildContext context, int index) {
              return Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                        context,
                        '/capacitacion_ins',
                        arguments: capacitacionesDisponibles[index],
                    );
                  },
                  child: Container(
                    width: 200.0,
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 0.5,
                          blurRadius: 5,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.local_hospital,
                              color: Color.fromRGBO(209, 77, 56, 1.0),
                            ),
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    'Cupos Restantes',
                                    style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(85, 85, 85, 1.0),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    capacitacionesDisponibles[index].disponibles.toString(),
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(100, 133, 16, 1.0),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          capacitacionesDisponibles[index].tema,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(85, 85, 85, 1.0),
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "Fecha de capacitación",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w300,
                                color: Color.fromRGBO(85, 85, 85, 1.0),
                              ),
                            ),
                            SizedBox(
                              width: 6,
                            ),
                            Text(
                              //"12/07/2020 . 08:00 a.m.",
                              DateFormat('dd/MM/yyyy . hh:mm a').format(
                                  capacitacionesDisponibles[index].fechaIni),
                              //CapacitacionesDisponibles[index]
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(85, 85, 85, 1.0),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 16,
              );
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}