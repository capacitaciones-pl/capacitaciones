part of 'capacitaciones_bloc.dart';

abstract class CapacitacionesState extends Equatable {
  const CapacitacionesState();
}

class Empty extends CapacitacionesState {
  @override
  List<Object> get props => [];
}

class Loading extends CapacitacionesState {
  @override
  List<Object> get props => [];
}

class Loaded extends CapacitacionesState  {
  final CapacitacionesDisponibles capacitacionesDisponibles;

  Loaded({
    @required this.capacitacionesDisponibles,
  });

  @override
  List<Object> get props => [
    capacitacionesDisponibles,
  ];
}

class Error extends CapacitacionesState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
