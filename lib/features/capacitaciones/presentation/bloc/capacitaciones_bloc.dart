import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/usecases/get_capacitaciones_disponibles.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'capacitaciones_event.dart';
part 'capacitaciones_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class CapacitacionesBloc extends Bloc<CapacitacionesEvent, CapacitacionesState> {
  final GetCapacitacionesDisponibles getCapacitacionesDisponibles;

  CapacitacionesBloc({
    @required GetCapacitacionesDisponibles getCapacitacionesDisponiblesCase,
  })  : assert(getCapacitacionesDisponiblesCase != null),
        getCapacitacionesDisponibles = getCapacitacionesDisponiblesCase,
        super(Empty());

  @override
  Stream<CapacitacionesState> mapEventToState(
    CapacitacionesEvent event,
  ) async* {
    if (event is GetCapacitacionesDataEvent) {
      yield Loading();

      final failureorCapacitacionesDisponibles =
          await getCapacitacionesDisponibles(Params(codCurso: event.codCurso));

      yield* failureorCapacitacionesDisponibles.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (data) async* {
          yield Loaded(capacitacionesDisponibles: data);
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
