part of 'capacitaciones_bloc.dart';

abstract class CapacitacionesEvent extends Equatable {
  const CapacitacionesEvent();
}

class GetCapacitacionesDataEvent extends CapacitacionesEvent {
  final String codCurso;

  GetCapacitacionesDataEvent(this.codCurso);

  @override
  List<Object> get props => [codCurso];
}
