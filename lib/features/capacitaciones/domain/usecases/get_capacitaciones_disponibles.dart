import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/repositories/capacitaciones_disponibles_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class GetCapacitacionesDisponibles implements UseCase<CapacitacionesDisponibles, Params> {
  final CapacitacionesDisponiblesRepository repository;

  GetCapacitacionesDisponibles(this.repository);

  @override
  Future<Either<Failure, CapacitacionesDisponibles>> call(Params params) async {
    return await repository.getCapacitacionesDisponibles(params.codCurso);
  }
}

class Params extends Equatable {
  final String codCurso;

  Params({@required this.codCurso});

  @override
  List<Object> get props => [codCurso];
}