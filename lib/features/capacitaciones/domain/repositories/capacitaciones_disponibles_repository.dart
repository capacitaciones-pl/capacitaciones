import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:dartz/dartz.dart';

abstract class CapacitacionesDisponiblesRepository {
  Future<Either<Failure, CapacitacionesDisponibles>> getCapacitacionesDisponibles(String codCurso);
}