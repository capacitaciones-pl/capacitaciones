import 'dart:convert';
import 'dart:io';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/mis_capacitaciones/presentation/bloc/mis_capacitaciones_bloc.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:app_capacitaciones/injection_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';
import 'package:http/http.dart' as http;


class MisCapacitacionesPage extends StatefulWidget {
  @override
  _MisCapacitacionesPageState createState() => _MisCapacitacionesPageState();
}

class _MisCapacitacionesPageState extends State<MisCapacitacionesPage> {
  String name;
  String path;
  String codPersona;

  @override
  void initState() {
    super.initState();
    SharedPreferences sharedPreferences = sL<SharedPreferences>();
    final jsonString = sharedPreferences.getString('CACHED_USER_DATA');
    UserDataModel userData = UserDataModel.fromJson(json.decode(jsonString));
    name = userData.nombres.split(' ')[0].capitalize() +
        " " +
        userData.apellidos.split(' ')[0].capitalize();
    codPersona = userData.codPersona;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sL<MisCapacitacionesBloc>()..add(GetMisCapacitacionesEvent()),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Mis Capacitaciones", true),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(
          index: 1,
          name: name,
        ),
        body: BlocBuilder<MisCapacitacionesBloc, MisCapacitacionesState>(
          builder: (context, state) {
            if (state is Loading) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is Loaded) {
              MisCapacitaciones misCapacitaciones = state.misCapacitaciones;
              return buildList(misCapacitaciones, context);
            } else {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Widget buildList(MisCapacitaciones misCapacitaciones, BuildContext context) {
    var capacitaciones = misCapacitaciones.data;
    return ListView.separated(
      padding: EdgeInsets.all(16),
      itemCount: misCapacitaciones.count,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          width: 200.0,
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.5,
                blurRadius: 5,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    Icons.collections_bookmark,
                    size: 26,
                    color: Color.fromRGBO(209, 77, 56, 1.0),
                  ),
                  Expanded(
                    child: _buildNota(capacitaciones[index].nota, capacitaciones[index].estado),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      capacitaciones[index].tema,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 14 / myTextScaleFactor,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(85, 85, 85, 1.0),
                      ),
                    ),
                  ),
                  /*Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        capacitaciones[index].nota.round() < 10 ? "Desaprobado" : "Aprobado",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),*/
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Text(
                    "Fecha de capacitación",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    DateFormat('dd/MM/yyyy').format(capacitaciones[index].fechaIni),
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  Visibility(
                    visible: capacitaciones[index].certificado,
                    child: Expanded(
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () async {
                            String url = '$MHSEC_CAP/capacitaciones/participante/certificado/$codPersona/' + capacitaciones[index].codCurso;
                            await _downloadFile(url, capacitaciones[index].codCurso, context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Icon(
                                Icons.file_download,
                                size: 22,
                                color: Color.fromRGBO(85, 85, 85, 1.0),
                              ),
                              Text(
                                'Certificado',
                                style: TextStyle(
                                  fontSize: 13 / myTextScaleFactor,
                                  fontWeight: FontWeight.w400,
                                  color: Color.fromRGBO(85, 85, 85, 1.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 16,
        );
      },
    );
  }

  Widget buildIcon(double nota) {
    if (nota == null) {
      return SizedBox(
        width: 12,
        height: 24,
      );
    }
    int notaI = nota.round();
    if (notaI == 20) {
      return Icon(
        Icons.sentiment_very_satisfied,
        color: Color.fromRGBO(100, 133, 16, 1.0),
      );
    } else if (notaI >= 0 && notaI <= 10) {
      return Icon(
        Icons.sentiment_dissatisfied,
        color: Color.fromRGBO(209, 77, 56, 1.0),
      );
    } else {
      return Icon(
        Icons.sentiment_satisfied,
        color: Color.fromRGBO(100, 133, 16, 1.0),
      );
    }
  }

  Future<File> _downloadFile(String url, String codCurso, BuildContext context) async {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text("Descargando el certificado..."),
      ),
    );
    http.Client client = new http.Client();
    var req = await client.get(Uri.parse(url));
    var bytes = req.bodyBytes;
    String dir = (await getExternalStorageDirectory()).path;
    File file = new File('$dir/Certificado$codCurso.pdf');
    await file.writeAsBytes(bytes);
    /*Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text("El certificado fue descargado."),
      ),
    );*/
    OpenFile.open('$dir/Certificado$codCurso.pdf');
    return file;
  }
  
  Widget _buildNota(double nota, String estado) {
    String notaStr = '';
    String notaS = '';
    Color color = Color.fromRGBO(209, 77, 56, 1.0);

    if (nota == null) {
      notaStr = estado;
      notaS = '-';
    } else {
      notaStr = estado;
      notaS = nota.round().toString();

      if (nota > 10)
        color = Color.fromRGBO(100, 133, 16, 1.0);
    }
    
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          notaStr,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color.fromRGBO(85, 85, 85, 1.0),
          ),
        ),
        SizedBox(
          width: 8,
        ),
        buildIcon(nota),
        SizedBox(
          width: 8,
        ),
        Text(
          notaS,
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w500,
            color: color,
          ),
        ),
      ],
    );
  }
}