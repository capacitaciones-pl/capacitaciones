part of 'mis_capacitaciones_bloc.dart';

abstract class MisCapacitacionesState extends Equatable {
  const MisCapacitacionesState();
}

class Empty extends MisCapacitacionesState {
  @override
  List<Object> get props => [];
}

class Loading extends MisCapacitacionesState{
  @override
  List<Object> get props => [];
}

class Loaded extends MisCapacitacionesState {
  final MisCapacitaciones misCapacitaciones;

  Loaded({
    @required this.misCapacitaciones,
  });

  @override
  List<Object> get props => [
    misCapacitaciones,
  ];
}

class Error extends MisCapacitacionesState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}


