part of 'mis_capacitaciones_bloc.dart';

abstract class MisCapacitacionesEvent extends Equatable {
  const MisCapacitacionesEvent();
}

class GetMisCapacitacionesEvent extends MisCapacitacionesEvent {
  @override
  List<Object> get props => [];
}
