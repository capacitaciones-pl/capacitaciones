import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_mis_capacitaciones.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'mis_capacitaciones_event.dart';
part 'mis_capacitaciones_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class MisCapacitacionesBloc extends Bloc<MisCapacitacionesEvent, MisCapacitacionesState> {
  final GetMisCapacitaciones getMisCapacitaciones;

  MisCapacitacionesBloc({
    @required GetMisCapacitaciones getMisCapacitacionesCase,
  })  : assert(getMisCapacitacionesCase != null),
        getMisCapacitaciones = getMisCapacitacionesCase,
        super(Empty());

  @override
  Stream<MisCapacitacionesState> mapEventToState(
    MisCapacitacionesEvent event,
  ) async* {
    yield Loading();
    final failureOrMisCapacitaciones = await getMisCapacitaciones(NoParams());
    yield* failureOrMisCapacitaciones.fold(
      (failure) async* {
        yield Error(message: _mapFailureToMessage(failure));
      },
      (data) async* {
        yield Loaded(
          misCapacitaciones: data,
        );
      },
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
