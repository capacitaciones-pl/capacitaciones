import 'dart:convert';

import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/data/models/capacitacion_inscripcion_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class CapacitacionInscripcionDataSource {
  Future<int> inscribirCapacitacion(String codPersona, String codCurso, String token);

  Future<int> desinscribirCapacitacion(String codPersona, String codCurso, String token);
}

class CapacitacionInscripcionDataSourceImpl implements CapacitacionInscripcionDataSource {
  final http.Client client;

  CapacitacionInscripcionDataSourceImpl({
    @required this.client,
  });

  @override
  Future<int> desinscribirCapacitacion(String codPersona, String codCurso, String token) async {
    var model = CapacitacionInscripcionModel(
      codPersona: codPersona,
      codCurso: codCurso,
    );

    final response = await client.post(
      'https://app.antapaccay.com.pe/proportaltest/mhsec_cap/api/v1/capacitaciones/inscripcion/delete',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(
        model.toJson(),
      ),
    );

    if (response.statusCode == 200) {
      return response.statusCode;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<int> inscribirCapacitacion(String codPersona, String codCurso, String token) async {
    var model = CapacitacionInscripcionModel(
      codPersona: codPersona,
      codCurso: codCurso,
    );

    final response = await client.post(
      'https://app.antapaccay.com.pe/proportaltest/mhsec_cap/api/v1/capacitaciones/inscripcion/createupdate',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(
        model.toJson(),
      ),
    );

    if (response.statusCode == 200) {
      return response.statusCode;
    } else {
      throw ServerException();
    }
  }
}