class CapacitacionInscripcionModel {
  final String codPersona;
  final String codCurso;

  CapacitacionInscripcionModel({
    this.codPersona,
    this.codCurso,
  });

  Map<String, dynamic> toJson() {
    return {
      'codPersona': codPersona,
      'codCurso': codCurso,
    };
  }
}