import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/data/datasources/capacitacion_inscripcion_data_source.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/repositories/capacitacion_inscripcion_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class CapacitacionInscripcionRepositoryImpl
implements CapacitacionInscripcionRepository {

  final CapacitacionInscripcionDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  CapacitacionInscripcionRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, void>> desinscribirCapacitacion(String codCurso) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final code = await remoteDataSource.desinscribirCapacitacion(
            localUserData.codPersona, codCurso, localUserData.token);
        return Right(code);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }

  @override
  Future<Either<Failure, int>> inscribirCapacitacion(String codCurso) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final code = await remoteDataSource.inscribirCapacitacion(
            localUserData.codPersona, codCurso, localUserData.token);
        return Right(code);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}