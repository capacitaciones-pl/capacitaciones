import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:dartz/dartz.dart';

abstract class CapacitacionInscripcionRepository {
  Future<Either<Failure, void>> inscribirCapacitacion(String codCurso);

  Future<Either<Failure, void>> desinscribirCapacitacion(String codCurso);
}