import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/repositories/capacitacion_inscripcion_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class DesinscribirCapacitacion implements UseCase<void, Params> {
  final CapacitacionInscripcionRepository repository;

  DesinscribirCapacitacion(this.repository);

  @override
  Future<Either<Failure, void>> call(Params params) async {
    return await repository.desinscribirCapacitacion(params.codCurso);
  }
}

class Params extends Equatable {
  final String codCurso;

  Params({
    @required this.codCurso,
  });

  @override
  List<Object> get props => [codCurso];
}