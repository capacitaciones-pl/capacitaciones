import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/repositories/capacitacion_inscripcion_repository.dart';
import 'package:dartz/dartz.dart';

import 'desinscribir_capacitacion.dart';

class InscribirCapacitacion implements UseCase<void, Params> {
  final CapacitacionInscripcionRepository repository;

  InscribirCapacitacion(this.repository);

  @override
  Future<Either<Failure, void>> call(Params params) async {
    return await repository.inscribirCapacitacion(params.codCurso);
  }
}