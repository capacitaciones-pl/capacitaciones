import 'package:app_capacitaciones/features/capacitacion_inscripcion/presentation/bloc/capacitacion_inscripcion_bloc.dart';
import 'package:app_capacitaciones/features/capacitaciones/domain/entities/capacitaciones_disponibles.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';

import '../../../../injection_container.dart';

class CapacitacionInscripcionPage extends StatefulWidget {
  @override
  _CapacitacionInscripcionPageState createState() => _CapacitacionInscripcionPageState();
}

class _CapacitacionInscripcionPageState extends State<CapacitacionInscripcionPage> {
  bool _show = true;

  @override
  Widget build(BuildContext context) {
    CapacitacionPorVencer curso = ModalRoute.of(context).settings.arguments;
    return BlocProvider(
      create: (context) => sL<CapacitacionInscripcionBloc>(),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Inscripción Capacitación", false),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(
          index: 1,
        ),
        body: BlocListener<CapacitacionInscripcionBloc, CapacitacionInscripcionState>(
          listener: (context, state) {
            if (state is Loading) {
              setState(() {
                _show = true;
              });
            }
          },
          child: BlocBuilder<CapacitacionInscripcionBloc, CapacitacionInscripcionState>(
            builder: (context, state) {
              if (state is Loading) {
                return Stack(
                  children: <Widget>[
                    buildList(curso, curso.inscrito, context),
                    Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ],
                );
              } else if (state is RegisterSuccess) {
                return Stack(
                  children: <Widget>[
                    buildList(curso, true, context),
                    Visibility(
                      visible: _show,
                      child: Container(
                        color: Colors.black.withAlpha(150),
                        child: Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 0),
                            padding: EdgeInsets.all(16),
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 0.5,
                                  blurRadius: 5,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'La inscripción para la capacitación se realizó correctamente.',
                                  style: TextStyle(
                                    fontSize: 14 / myTextScaleFactor,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(85, 85, 85, 1.0),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _show = false;
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Color.fromRGBO(100, 133, 16, 1.0),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 0.5,
                                            blurRadius: 5,
                                            offset: Offset(0, 3), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: Center(
                                        child: Text(
                                          'Aceptar',
                                          style: TextStyle(
                                            fontSize: 16 / myTextScaleFactor,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else if (state is UnregisterSuccess) {
                return Stack(
                  children: <Widget>[
                    buildList(curso, false, context),
                    Visibility(
                      visible: _show,
                      child: Container(
                        color: Colors.black.withAlpha(150),
                        child: Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 0),
                            padding: EdgeInsets.all(16),
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 0.5,
                                  blurRadius: 5,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'El retiro de su inscripción para la capacitación se realizó correctamente.',
                                  style: TextStyle(
                                    fontSize: 14 / myTextScaleFactor,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromRGBO(85, 85, 85, 1.0),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _show = false;
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Color.fromRGBO(100, 133, 16, 1.0),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 0.5,
                                            blurRadius: 5,
                                            offset: Offset(0, 3), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: Center(
                                        child: Text(
                                          'Aceptar',
                                          style: TextStyle(
                                            fontSize: 16 / myTextScaleFactor,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else if (state is Initial) {
                return Stack(
                  children: <Widget>[
                    buildList(curso, curso.inscrito, context),
                  ],
                );
              } else {
                return Container(
                  color: Colors.blue,
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Widget buildList(CapacitacionPorVencer curso, bool inscrito, BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(16),
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.5,
                blurRadius: 5,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    curso.enlace != null && curso.enlace.length > 0 ?
                    Icons.ondemand_video :
                    Icons.collections_bookmark,
                    color: Color.fromRGBO(209, 77, 56, 1.0),
                    size: 26,
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color.fromRGBO(100, 133, 16, 1.0),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                curso.tema,
                style: TextStyle(
                  fontSize: 15 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.calendar_today,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Fecha programada",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        // "12/07/2020 . 08:00 a.m.",
                        DateFormat("dd/MM/yyyy . hh:mm a")
                            .format(curso.fechaIni),
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.calendar_today,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Fecha finalización",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        // "12/07/2020 . 08:00 a.m.",
                        DateFormat("dd/MM/yyyy . hh:mm a")
                            .format(curso.fechaFinal),
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.timer,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Duración",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        curso.duracion + ' horas(s)',
                        //curso.,
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.event_available,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Vigencia",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        // "1 año",
                        curso.vigenciatxt,
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.assignment_ind,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Expositor",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        //"Elena María Arces",
                        curso.expositores.isEmpty
                            ? 'No definido'
                            : curso.expositores[0].nombre.capitalize(),
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.business,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Empresa capacitadora",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        curso.empresaCap,
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.airline_seat_recline_normal,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                    size: 16,
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Text(
                    "Cupos disponibles",
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        curso.disponibles.toString(),
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              var bloc = context.bloc<CapacitacionInscripcionBloc>();
              if (inscrito) {
                bloc.add(UnregisterEvent(curso.codCurso));
              } else {
                bloc.add(RegisterEvent(curso.codCurso));
              }
            },
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: inscrito ? Color.fromRGBO(209, 77, 56, 1.0) : Color.fromRGBO(100, 133, 16, 1.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 0.5,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Center(
                child: Text(
                  inscrito ? "Retirar inscripción" : "Inscribirse",
                  style: TextStyle(
                    fontSize: 17 / myTextScaleFactor,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
