import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/usecases/desinscribir_capacitacion.dart';
import 'package:app_capacitaciones/features/capacitacion_inscripcion/domain/usecases/inscribir_capacitacion.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'capacitacion_inscripcion_event.dart';
part 'capacitacion_inscripcion_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class CapacitacionInscripcionBloc extends Bloc<CapacitacionInscripcionEvent, CapacitacionInscripcionState> {
  final DesinscribirCapacitacion desinscribirCapacitacion;
  final InscribirCapacitacion inscribirCapacitacion;

  CapacitacionInscripcionBloc({
    @required DesinscribirCapacitacion desinscribirCapacitacionCase,
    @required InscribirCapacitacion inscribirCapacitacionCase,
  })
      : assert(desinscribirCapacitacionCase != null),
        assert(inscribirCapacitacionCase != null),
        desinscribirCapacitacion = desinscribirCapacitacionCase,
        inscribirCapacitacion = inscribirCapacitacionCase,
        super(Initial());

  @override
  Stream<CapacitacionInscripcionState> mapEventToState(
    CapacitacionInscripcionEvent event,
  ) async* {
    if (event is RegisterEvent) {
      yield Loading();

      final failureOrCode =
          await inscribirCapacitacion(Params(codCurso: event.codCurso));

      yield* failureOrCode.fold(
        (failure) async* {
          yield Error(
            message: _mapFailureToMessage(failure),
          );
        },
        (data) async* {
          yield RegisterSuccess();
        },
      );
    } else if (event is UnregisterEvent) {
      yield Loading();

      final failureOrCode =
      await desinscribirCapacitacion(Params(codCurso: event.codCurso));

      yield* failureOrCode.fold(
        (failure) async* {
          yield Error(
            message: _mapFailureToMessage(failure),
          );
        },
        (data) async* {
          yield UnregisterSuccess();
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
