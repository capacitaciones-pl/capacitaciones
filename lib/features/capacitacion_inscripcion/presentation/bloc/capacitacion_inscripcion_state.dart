part of 'capacitacion_inscripcion_bloc.dart';

abstract class CapacitacionInscripcionState extends Equatable {
  const CapacitacionInscripcionState();
}

class Initial extends CapacitacionInscripcionState {
  @override
  List<Object> get props => [];
}

class Loading extends CapacitacionInscripcionState {
  @override
  List<Object> get props => [];
}

class RegisterSuccess extends CapacitacionInscripcionState {
  @override
  List<Object> get props => [];
}

class UnregisterSuccess extends CapacitacionInscripcionState {
  @override
  List<Object> get props => [];
}

class Error extends CapacitacionInscripcionState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
