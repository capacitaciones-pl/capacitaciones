part of 'capacitacion_inscripcion_bloc.dart';

abstract class CapacitacionInscripcionEvent extends Equatable {
  const CapacitacionInscripcionEvent();
}

class RegisterEvent extends CapacitacionInscripcionEvent {
  final String codCurso;

  RegisterEvent(this.codCurso);

  @override
  List<Object> get props => [codCurso];
}

class UnregisterEvent extends CapacitacionInscripcionEvent {
  final String codCurso;

  UnregisterEvent(this.codCurso);

  @override
  List<Object> get props => [codCurso];
}
