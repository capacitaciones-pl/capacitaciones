import 'package:app_capacitaciones/features/evaluacion/domain/entities/preguntas_curso.dart';
import 'package:meta/meta.dart';

class PreguntasCursoModel extends PreguntasCurso {
  PreguntasCursoModel({
    @required int count,
    @required List<PreguntaCurso> data,
  }) : super(
          count: count,
          data: data,
        );

  factory PreguntasCursoModel.fromJson(Map<String, dynamic> json){
    return PreguntasCursoModel(
      count: json['count'],
      data: (json['data'] as List).map((e) => PreguntaCursoModel.fromJson(e)).toList(),
    );
  }
}

class PreguntaCursoModel extends PreguntaCurso {
  PreguntaCursoModel({
    @required String codCurso,
    @required int codPregunta,
    @required String descripcion,
    @required String tipo,
    @required int puntaje,
    @required bool estado,
    @required int countRespuestas,
    @required int codAlternativaEscogida,
    @required String respuestaTextual,
    @required List<AlternativaPregunta> alternativas,
}) : super (
    codCurso: codCurso,
    codPregunta: codPregunta,
    descripcion: descripcion,
    tipo: tipo,
    puntaje: puntaje,
    estado: estado,
    countRespuestas: countRespuestas,
    codAlternativaEscogida: codAlternativaEscogida,
    respuestaTextual: respuestaTextual,
    alternativas: alternativas,
  );

  factory PreguntaCursoModel.fromJson(Map<String, dynamic> json){
    return PreguntaCursoModel(
      codCurso: json['codCurso'],
      codPregunta: json['codPregunta'],
      descripcion: json['descripcion'],
      tipo: json['tipo'],
      puntaje: json['puntaje'],
      estado: json['estado'],
      countRespuestas: json['countRespuestas'],
      codAlternativaEscogida: -1,
      alternativas: (json['alternativas'] as List).map((e) => AlternativaPreguntaModel.fromJson(e)).toList(),
    );
  }
}

class AlternativaPreguntaModel extends AlternativaPregunta {
  AlternativaPreguntaModel({
    @required int codAlternativa,
    @required int codPregunta,
    @required String descripcion,
    @required bool seleccionada,
  }) : super(
          codAlternativa: codAlternativa,
          codPregunta: codPregunta,
          descripcion: descripcion,
          seleccionada: seleccionada,
        );

  factory AlternativaPreguntaModel.fromJson(Map<String, dynamic> json) {
    return AlternativaPreguntaModel(
      codAlternativa: json['codAlternativa'],
      codPregunta: json['codPregunta'],
      descripcion: json['descripcion'],
      seleccionada: false,
    );
  }
}