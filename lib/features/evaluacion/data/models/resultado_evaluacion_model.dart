import 'package:app_capacitaciones/features/evaluacion/domain/entities/resultado_evaluacion.dart';
import 'package:meta/meta.dart';

class ResultadoEvaluacionModel extends ResultadoEvaluacion {
  ResultadoEvaluacionModel({
    @required double nota,
    @required String estadoEvaluacion,
    @required int porcentajeEval,
    @required double puntajeTotal,
  }) : super(
          nota: nota,
          estadoEvaluacion: estadoEvaluacion,
          porcentajeEval: porcentajeEval,
          puntajeTotal: puntajeTotal,
        );

  factory ResultadoEvaluacionModel.fromJson(Map<String, dynamic> json) {

    return ResultadoEvaluacionModel(
      nota: json['nota'] is int ? (json['nota'] as int).toDouble() : json['nota'],
      estadoEvaluacion: json['estadoEvaluacion'],
      porcentajeEval: json['porcentajeEval'],
      puntajeTotal: json['puntajeTotal'],
    );
  }
}