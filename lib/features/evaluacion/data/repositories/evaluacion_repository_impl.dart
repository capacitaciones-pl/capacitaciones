import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/evaluacion/data/datasources/evaluacion_data_source.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/respuestas_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/resultado_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/repositories/evaluacion_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class EvaluacionRepositoryImpl implements EvaluacionRepository {
  final EvaluacionDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  EvaluacionRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, ResultadoEvaluacion>> sendRespuestas(String codCurso, List<RespuestaEvaluacion> respuestas) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();

        RespuestasEvaluacion respuestasEvaluacion = RespuestasEvaluacion(
          codCurso: codCurso,
          codPersona: localUserData.codPersona,
          data: respuestas,
        );

        final remoteUserData = await remoteDataSource.sendRespuestas(
          respuestasEvaluacion,
          localUserData.token,
        );

        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}