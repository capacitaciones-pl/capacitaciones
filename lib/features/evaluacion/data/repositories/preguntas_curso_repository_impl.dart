import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/evaluacion/data/datasources/evaluacion_data_source.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/preguntas_curso.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/repositories/preguntas_curso_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class PreguntasCursoRepositoryImpl implements PreguntasCursoRepository {
  final EvaluacionDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  PreguntasCursoRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, PreguntasCurso>> getPreguntas(String codCurso) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final remoteUserData = await remoteDataSource.getPreguntas(
            codCurso, localUserData.token);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}
