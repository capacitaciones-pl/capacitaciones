import 'dart:convert';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/evaluacion/data/models/preguntas_curso_model.dart';
import 'package:app_capacitaciones/features/evaluacion/data/models/resultado_evaluacion_model.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/respuestas_evaluacion.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class EvaluacionDataSource {
  Future<PreguntasCursoModel> getPreguntas(String codCurso, String token);
  Future<ResultadoEvaluacionModel> sendRespuestas(RespuestasEvaluacion respuestas, String token);
}

class EvaluacionDataSourceImpl implements EvaluacionDataSource {
  final http.Client client;

  EvaluacionDataSourceImpl({
    @required this.client
  });

  @override
  Future<PreguntasCursoModel> getPreguntas(String codCurso, String token) async {
    final response = await client.get(
      '$MHSEC_CAP/capacitaciones/pregunta/getconrpta/$codCurso',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return PreguntasCursoModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<ResultadoEvaluacionModel> sendRespuestas(RespuestasEvaluacion respuestas, String token) async {
    final response = await client.post(
      '$MHSEC_CAP/capacitaciones/cursos/evaluacionpreguntasexamen',
      body: json.encode(respuestas.toJson()),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return ResultadoEvaluacionModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}