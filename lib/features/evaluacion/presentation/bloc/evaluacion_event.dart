part of 'evaluacion_bloc.dart';

abstract class EvaluacionEvent extends Equatable {
  const EvaluacionEvent();
}

class GetPreguntasCursoEvent extends EvaluacionEvent {
  final String codCurso;

  GetPreguntasCursoEvent(this.codCurso);

  @override
  List<Object> get props => [codCurso];
}

class SendRespuestasEvent extends EvaluacionEvent {
  final String codCurso;
  final PreguntasCurso preguntasCurso;

  SendRespuestasEvent(this.codCurso, this.preguntasCurso);

  @override
  List<Object> get props => [codCurso, preguntasCurso];
}
