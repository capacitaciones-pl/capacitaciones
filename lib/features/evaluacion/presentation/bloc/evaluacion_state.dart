part of 'evaluacion_bloc.dart';

abstract class EvaluacionState extends Equatable {
  const EvaluacionState();
}

class Empty extends EvaluacionState {
  @override
  List<Object> get props => [];
}

class Loading extends EvaluacionState {
  @override
  List<Object> get props => [];
}

class Loaded extends EvaluacionState {
  final PreguntasCurso preguntasCurso;

  Loaded({
    @required this.preguntasCurso,
  });

  @override
  List<Object> get props => [preguntasCurso];
}

class Evaluated extends EvaluacionState {
  final ResultadoEvaluacion resultadoEvaluacion;

  Evaluated({
    @required this.resultadoEvaluacion,
  });

  @override
  List<Object> get props => [resultadoEvaluacion];
}

class Error extends EvaluacionState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}

class ErrorResultado extends EvaluacionState {
  final String message;

  ErrorResultado({@required this.message});

  @override
  List<Object> get props => [message];
}

class LoadingResult extends EvaluacionState {
  @override
  List<Object> get props => [];
}
