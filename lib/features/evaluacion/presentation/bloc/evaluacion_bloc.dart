import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/preguntas_curso.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/respuestas_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/resultado_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/usecases/get_preguntas.dart' as UseCase1;
import 'package:app_capacitaciones/features/evaluacion/domain/usecases/send_respuestas.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'evaluacion_event.dart';
part 'evaluacion_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class EvaluacionBloc extends Bloc<EvaluacionEvent, EvaluacionState> {
  final UseCase1.GetPreguntas getPreguntas;
  final SendRespuestas sendRespuestas;

  EvaluacionBloc({
    @required UseCase1.GetPreguntas getPreguntasCase,
    @required SendRespuestas sendRespuestasCase,
  })  : assert(getPreguntasCase != null),
        assert(sendRespuestasCase != null),
        sendRespuestas = sendRespuestasCase,
        getPreguntas = getPreguntasCase,
        super(Empty());

  @override
  Stream<EvaluacionState> mapEventToState(
    EvaluacionEvent event,
  ) async* {
    if (event is GetPreguntasCursoEvent) {
      yield Loading();

      final failureOrData =
          await getPreguntas(UseCase1.Params(codCurso: event.codCurso));

      yield* failureOrData.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (data) async* {
          if (data.count > 0) {
            yield Loaded(
              preguntasCurso: data,
            );
          } else {
            yield Empty();
          }
        },
      );
    } else if (event is SendRespuestasEvent) {
      yield LoadingResult();

      List<RespuestaEvaluacion> respuestas = new List<RespuestaEvaluacion>();
      event.preguntasCurso.data.forEach(
        (e) {
          if (e.tipo.compareTo("1") == 0) {
            respuestas.add(
              RespuestaEvaluacion(
                codPregunta: e.codPregunta,
                tipo: e.tipo,
                respuesta: e.codAlternativaEscogida.toString(),
              ),
            );
          } else if (e.tipo.compareTo("3") == 0) {
            respuestas.add(
              RespuestaEvaluacion(
                codPregunta: e.codPregunta,
                tipo: e.tipo,
                respuesta: e.respuestaTextual,
              ),
            );
          } else if (e.tipo.compareTo("2") == 0) {
            List<int> alternativas = new List<int>();
            e.alternativas.forEach(
              (a) {
                if (a.seleccionada) {
                  alternativas.add(a.codAlternativa);
                }
              },
            );
            respuestas.add(
              RespuestaEvaluacion(
                codPregunta: e.codPregunta,
                tipo: e.tipo,
                respuesta: alternativas.join(';'),
              ),
            );
          }
        },
      );

      final failureOrData = await sendRespuestas(
        Params(
          codCurso: event.codCurso,
          respuestas: respuestas,
        ),
      );

      yield* failureOrData.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (data) async* {
          yield Evaluated(resultadoEvaluacion: data);
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
