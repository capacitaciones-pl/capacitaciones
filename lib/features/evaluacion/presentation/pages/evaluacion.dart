import 'package:app_capacitaciones/features/evaluacion/domain/entities/preguntas_curso.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/resultado_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/presentation/bloc/evaluacion_bloc.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:app_capacitaciones/injection_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';

class EvaluacionPage extends StatefulWidget {
  @override
  _EvaluacionPageState createState() => _EvaluacionPageState();
}

class _EvaluacionPageState extends State<EvaluacionPage> {
  bool _isLoading = true;
  bool _isEmpty = false;
  PreguntasCurso _preguntasCurso;
  int _totalPreguntas;
  int _idxPregunta;
  PreguntaCurso _preguntaActual;
  TextEditingController _controller = new TextEditingController();
  ProximaCapacitacion _curso;
  ResultadoEvaluacion _resultadoEvaluacion;
  bool _isEvaluated = false;
  bool _isLoadingResult = false;
  BuildContext _buildContext;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _curso = ModalRoute.of(context).settings.arguments;
    return BlocProvider(
      create: (context) => sL<EvaluacionBloc>()..add(GetPreguntasCursoEvent(_curso.codCurso)),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Evaluación", false),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(
          index: 1,
        ),
        body: BlocListener<EvaluacionBloc, EvaluacionState>(
          listener: (context, state) {
            setState(() {
              _buildContext = context;
            });
            if (state is Loading) {
              setState(() {
                _isLoading = true;
              });
            } else if (state is Loaded) {
              setState(() {
                _isLoading = false;
                _preguntasCurso = state.preguntasCurso;
                _totalPreguntas = _preguntasCurso.count;
                if (_totalPreguntas > 0) {
                  _idxPregunta = 0;
                  _preguntaActual = _preguntasCurso.data[_idxPregunta];
                }
              });
            } else if (state is Evaluated) {
              _resultadoEvaluacion = state.resultadoEvaluacion;
              _isEvaluated = true;
              _isLoadingResult = false;
            } else if (state is LoadingResult) {
              _isLoadingResult = true;
            } else if (state is Empty) {
              setState(() {
                _isEmpty = true;
                _isLoading = false;
              });
            }
          },
          child: ListView(
            padding: EdgeInsets.all(16),
            children: createItems(context),
          ),
        ),
      ),
    );
  }

  List<Widget> createItems(BuildContext context) {
    List<Widget> widgets = new List<Widget>();
    widgets.add(createTitle());
    widgets.add(
      SizedBox(
        height: 16,
      ),
    );
    if (_isEmpty) {
      widgets.add(
        Text(
          'No se han definido preguntas.',
          style: TextStyle(
            fontSize: 14 / myTextScaleFactor,
            fontWeight: FontWeight.w400,
            color: Color.fromRGBO(85, 85, 85, 1.0),
          ),
        ),
      );
    }
    else if (_isLoading) {
      widgets.add(
        Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    } else if (_isLoadingResult) {
      widgets.add(
        Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    } else if (_isEvaluated) {
      widgets.add(_buildResultMessage());
    }
    else {
      widgets.add(_buildPregunta());
      widgets.add(
        SizedBox(
          height: 16,
        ),
      );
      widgets.add(_buildPagination());
      widgets.add(
        SizedBox(
          height: 16,
        ),
      );
      widgets.add(_buildFinalizacion(context));
    }

    return widgets;
  }

  Widget createTitle() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.collections_bookmark,
            color: Color.fromRGBO(209, 77, 56, 1.0),
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Text(
              _curso.tema,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 15 / myTextScaleFactor,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(85, 85, 85, 1.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPregunta() {
    int puntaje = _preguntaActual.puntaje;
    int nroPregunta = _idxPregunta + 1;
    List<Widget> widgets = new List<Widget>();
    widgets.add(
      Text(
        'Pregunta $nroPregunta de $_totalPreguntas (Vale $puntaje puntos)',
        style: TextStyle(
          fontSize: 15 / myTextScaleFactor,
          fontWeight: FontWeight.w400,
          color: Color.fromRGBO(209, 77, 56, 1.0),
        ),
      ),
    );
    widgets.add(
      SizedBox(
        height: 8,
      ),
    );
    widgets.add(
      Html(
        data: _preguntaActual.descripcion,
      ),
      /*Text(
        _preguntaActual.descripcion,
        style: TextStyle(
          fontSize: 14 / myTextScaleFactor,
          fontWeight: FontWeight.w300,
          color: Color.fromRGBO(85, 85, 85, 1.0),
        ),
      ),*/
    );
    widgets.add(
      SizedBox(
        height: 8,
      ),
    );
    widgets.addAll(_buildAlternativas());

    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
    );
  }

  List<Widget> _buildAlternativas() {
    List<Widget> alternativas = new List<Widget>();

    if (_preguntaActual.tipo.compareTo("3") == 0) {
      _controller.text = _preguntaActual.respuestaTextual;
      alternativas.add(
        SizedBox(
          height: 8,
        ),
      );
      alternativas.add(
        Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.black12,
          ),
          child: TextField(
            maxLines: 4,
            controller: _controller,
            decoration: InputDecoration.collapsed(
              hintText: "Ingrese su respuesta aquí.",
            ),
            style: TextStyle(
              fontSize: 14 / myTextScaleFactor,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(85, 85, 85, 1.0),
            ),
            onChanged: (value) {
              _preguntaActual.respuestaTextual = value;
            },
          ),
        ),
      );
    } else {
      _preguntaActual.alternativas.forEach(
            (element) {
          if (_preguntaActual.tipo.compareTo("1") == 0) {
            alternativas.add(_buildAlternativa1(element));
          } else if (_preguntaActual.tipo.compareTo("2") == 0) {
            alternativas.add(_buildAlternativa2(element));
          }
        },
      );
    }
    return alternativas;
  }

  Widget _buildAlternativa1(AlternativaPregunta alternativa) {
    return Row(
      children: <Widget>[
        Radio(
          value: alternativa.codAlternativa,
          groupValue: _preguntaActual.codAlternativaEscogida,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          activeColor: Color.fromRGBO(100, 133, 16, 1.0),
          onChanged: (int value) {
            setState(() {
              _preguntaActual.codAlternativaEscogida = value;
            });
          },
        ),
        Expanded(
          child: Text(
            alternativa.descripcion,
            style: TextStyle(
              fontSize: 14 / myTextScaleFactor,
              fontWeight: FontWeight.w300,
              color: Color.fromRGBO(85, 85, 85, 1.0),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildAlternativa2(AlternativaPregunta alternativa) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: alternativa.seleccionada,
          activeColor: Color.fromRGBO(100, 133, 16, 1.0),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          onChanged: (bool value) {
            setState(() {
              alternativa.seleccionada = value;
            });
          },
        ),
        Expanded(
          child: Text(
            alternativa.descripcion,
            style: TextStyle(
              fontSize: 14 / myTextScaleFactor,
              fontWeight: FontWeight.w300,
              color: Color.fromRGBO(85, 85, 85, 1.0),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPagination() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  setState(
                    () {
                      if (_idxPregunta > 0 && _totalPreguntas > 0) {
                        _idxPregunta = _idxPregunta - 1;
                        _preguntaActual = _preguntasCurso.data[_idxPregunta];
                      }
                      _controller.clear();
                    },
                  );
                },
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.keyboard_arrow_left,
                      color: Color.fromRGBO(209, 77, 56, 1.0),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Pregunta anterior",
                      style: TextStyle(
                        fontSize: 14 / myTextScaleFactor,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(85, 85, 85, 1.0),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  setState(
                    () {
                      if (_totalPreguntas > 0 &&
                          _idxPregunta + 1 < _totalPreguntas) {
                        _idxPregunta = _idxPregunta + 1;
                        _preguntaActual = _preguntasCurso.data[_idxPregunta];
                      }
                      _controller.clear();
                    },
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Pregunta siguiente",
                      style: TextStyle(
                        fontSize: 14 / myTextScaleFactor,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(85, 85, 85, 1.0),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: Color.fromRGBO(209, 77, 56, 1.0),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFinalizacion(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          _showAlert(context);
        },
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Color.fromRGBO(100, 133, 16, 1.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.5,
                blurRadius: 5,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
            child: Text(
              "Finalizar Evaluación",
              style: TextStyle(
                fontSize: 17 / myTextScaleFactor,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _showAlert(BuildContext context) {
    // set up the button
    //Navigator.of(context).pop(); // dismiss dialog
    Widget okButton = Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          BlocProvider.of<EvaluacionBloc>(_buildContext).add(
            SendRespuestasEvent(
              _curso.codCurso,
              _preguntasCurso,
            ),
          );
          Navigator.of(context).pop();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 14),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Color.fromRGBO(100, 133, 16, 1.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.5,
                blurRadius: 5,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Text('Aceptar',style: TextStyle(
            fontSize: 15 / myTextScaleFactor,
            fontWeight: FontWeight.w300,
            color: Colors.white,
          ),),
        ),
      ),
    );

    Widget calcelButton = Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 14),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.transparent,
          ),
          child: Text('Cancelar',style: TextStyle(
            fontSize: 15 / myTextScaleFactor,
            fontWeight: FontWeight.w300,
            color: Color.fromRGBO(85, 85, 85, 1.0),
          ),),
        ),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        'Finalizar exámen',
        style: TextStyle(
          fontSize: 15 / myTextScaleFactor,
          fontWeight: FontWeight.w400,
          color: Color.fromRGBO(85, 85, 85, 1.0),
        ),
      ),
      content: Text(
        '¿Está seguro que desea finalizar la evaluación?',
        style: TextStyle(
          fontSize: 14 / myTextScaleFactor,
          fontWeight: FontWeight.w300,
          color: Color.fromRGBO(85, 85, 85, 1.0),
        ),
      ),
      actions: [
        calcelButton,
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _buildResultMessage() {
    double nota = _resultadoEvaluacion.nota;
    double puntos = 0;

    _preguntasCurso.data.forEach((element) {
      if (element.tipo.compareTo("3") == 0) {
        puntos = puntos + element.puntaje;
      }
    });

    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Resultados evaluación',
            style: TextStyle(
              fontSize: 15 / myTextScaleFactor,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(85, 85, 85, 1.0),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            _resultadoEvaluacion.estadoEvaluacion.compareTo('Pendiente') == 0
                ? 'Su nota parcial es $nota. El capacitador calificará las preguntas abiertas equivalentes a $puntos puntos.'
                : 'Su nota final es $nota',
            style: TextStyle(
              fontSize: 14 / myTextScaleFactor,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(85, 85, 85, 1.0),
            ),
          ),
        ],
      ),
    );
  }
}
