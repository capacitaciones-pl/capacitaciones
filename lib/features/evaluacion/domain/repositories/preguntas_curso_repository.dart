import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/preguntas_curso.dart';
import 'package:dartz/dartz.dart';

abstract class PreguntasCursoRepository {
  Future<Either<Failure, PreguntasCurso>> getPreguntas(String codCurso);
}