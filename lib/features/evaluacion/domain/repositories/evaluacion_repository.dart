import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/respuestas_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/resultado_evaluacion.dart';
import 'package:dartz/dartz.dart';

abstract class EvaluacionRepository {
  Future<Either<Failure, ResultadoEvaluacion>> sendRespuestas(String codCurso, List<RespuestaEvaluacion> respuestas);
}