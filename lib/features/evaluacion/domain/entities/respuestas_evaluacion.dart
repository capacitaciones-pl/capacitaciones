import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RespuestasEvaluacion extends Equatable {
  final String codCurso;
  final String codPersona;
  final List<RespuestaEvaluacion> data;

  RespuestasEvaluacion({
    @required this.codCurso,
    @required this.codPersona,
    @required this.data,
  });

  @override
  List<Object> get props => [
    codCurso,
    codPersona,
    data,
  ];

  Map<String, dynamic> toJson() {
    return {
      'codCurso': codCurso,
      'codPersona': codPersona,
      'data': data != null ? data.map((e) => e.toJson()).toList() : null,
    };
  }
}

class RespuestaEvaluacion extends Equatable {
  final int codPregunta;
  final String tipo;
  final String respuesta;

  RespuestaEvaluacion({
    @required this.codPregunta,
    @required this.tipo,
    @required this.respuesta,
  });

  Map<String, dynamic> toJson() {
    return {
      'codPregunta': codPregunta,
      'tipo': tipo,
      'respuesta': respuesta,
    };
  }

  @override
  List<Object> get props => [codPregunta, tipo, respuesta,];
}
