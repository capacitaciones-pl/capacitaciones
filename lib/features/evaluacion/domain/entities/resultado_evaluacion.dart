import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ResultadoEvaluacion extends Equatable {
  final double nota;
  final String estadoEvaluacion;
  final int porcentajeEval;
  final double puntajeTotal;

  ResultadoEvaluacion({
    @required this.nota,
    @required this.estadoEvaluacion,
    @required this.porcentajeEval,
    @required this.puntajeTotal,
  });

  @override
  List<Object> get props => [nota, estadoEvaluacion, porcentajeEval, puntajeTotal,];
}