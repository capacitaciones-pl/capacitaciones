import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class PreguntasCurso extends Equatable {
  final int count;
  final List<PreguntaCurso> data;

  PreguntasCurso({
    @required this.count,
    @required this.data,
  });

  @override
  List<Object> get props => [count, data];
}

class PreguntaCurso extends Equatable {
  final String codCurso;
  final int codPregunta;
  final String descripcion;
  final String tipo;
  final int puntaje;
  final bool estado;
  final int countRespuestas;
  int codAlternativaEscogida;
  String respuestaTextual;
  final List<AlternativaPregunta> alternativas;

  PreguntaCurso({
    @required this.codCurso,
    @required this.codPregunta,
    @required this.descripcion,
    @required this.tipo,
    @required this.puntaje,
    @required this.estado,
    @required this.countRespuestas,
    @required this.codAlternativaEscogida,
    @required this.respuestaTextual,
    @required this.alternativas,
  });

  @override
  List<Object> get props => [
    codCurso,
    codPregunta,
    descripcion,
    tipo,
    puntaje,
    estado,
    countRespuestas,
    alternativas,
  ];
}

class AlternativaPregunta extends Equatable {
  final int codAlternativa;
  final int codPregunta;
  final String descripcion;
  bool seleccionada;

  AlternativaPregunta({
    @required this.codAlternativa,
    @required this.codPregunta,
    @required this.descripcion,
    @required this.seleccionada,
  });

  @override
  List<Object> get props => [codAlternativa, codPregunta, descripcion];
}