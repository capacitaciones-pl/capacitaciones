import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/preguntas_curso.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/repositories/preguntas_curso_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class GetPreguntas implements UseCase<PreguntasCurso, Params> {
  final PreguntasCursoRepository repository;

  GetPreguntas(this.repository);

  @override
  Future<Either<Failure, PreguntasCurso>> call(Params params) async {
    return await repository.getPreguntas(params.codCurso);
  }
}

class Params extends Equatable {
  final String codCurso;

  Params({
    @required this.codCurso,
  });

  @override
  List<Object> get props => [codCurso];
}