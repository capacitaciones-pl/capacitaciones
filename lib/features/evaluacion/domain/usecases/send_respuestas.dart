import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/respuestas_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/entities/resultado_evaluacion.dart';
import 'package:app_capacitaciones/features/evaluacion/domain/repositories/evaluacion_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SendRespuestas implements UseCase<ResultadoEvaluacion, Params> {
  final EvaluacionRepository repository;

  SendRespuestas(this.repository);

  @override
  Future<Either<Failure, ResultadoEvaluacion>> call(Params params) async {
    return await repository.sendRespuestas(params.codCurso, params.respuestas);
  }
}

class Params extends Equatable {
  final String codCurso;
  final List<RespuestaEvaluacion> respuestas;

  Params({
    @required this.codCurso,
    @required this.respuestas,
  });

  @override
  List<Object> get props => [codCurso, respuestas,];
}