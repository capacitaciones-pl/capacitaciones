import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_proximas_capacitaciones.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'capacitaciones_programadas_event.dart';
part 'capacitaciones_programadas_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class CapacitacionesProgramadasBloc extends Bloc<CapacitacionesProgramadasEvent, CapacitacionesProgramadasState> {
  final GetProximasCapacitaciones getProximasCapacitaciones;

  CapacitacionesProgramadasBloc({
    @required GetProximasCapacitaciones getProximasCapacitacionesCase,
  })  : assert(getProximasCapacitacionesCase != null),
        getProximasCapacitaciones = getProximasCapacitacionesCase,
        super(Empty());

  @override
  Stream<CapacitacionesProgramadasState> mapEventToState(
    CapacitacionesProgramadasEvent event,
  ) async* {
    if (event is GetCapacitacionesProgramadasEvent) {
      yield Loading();

      final failureOrProximasCapacitaciones =
          await getProximasCapacitaciones(NoParams());

      yield* failureOrProximasCapacitaciones.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (data) async* {
          yield Loaded(
            proximasCapacitaciones: data,
          );
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
