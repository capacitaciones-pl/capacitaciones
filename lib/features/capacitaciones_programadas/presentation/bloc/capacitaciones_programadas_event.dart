part of 'capacitaciones_programadas_bloc.dart';

abstract class CapacitacionesProgramadasEvent extends Equatable {
  const CapacitacionesProgramadasEvent();
}

class GetCapacitacionesProgramadasEvent extends CapacitacionesProgramadasEvent {
  @override
  List<Object> get props => [];
}
