part of 'capacitaciones_programadas_bloc.dart';

abstract class CapacitacionesProgramadasState extends Equatable {
  const CapacitacionesProgramadasState();
}

class Empty extends CapacitacionesProgramadasState {
  @override
  List<Object> get props => [];
}

class Loading extends CapacitacionesProgramadasState {
  @override
  List<Object> get props => [];
}

class Loaded extends CapacitacionesProgramadasState {
  final ProximasCapacitaciones proximasCapacitaciones;

  Loaded({
    @required this.proximasCapacitaciones,
  });

  @override
  List<Object> get props => [
    proximasCapacitaciones,
  ];
}

class Error extends CapacitacionesProgramadasState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
