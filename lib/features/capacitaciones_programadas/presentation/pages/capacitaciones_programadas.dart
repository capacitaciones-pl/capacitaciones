import 'dart:convert';

import 'package:app_capacitaciones/features/capacitaciones_programadas/presentation/bloc/capacitaciones_programadas_bloc.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:app_capacitaciones/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';

class CapacitacionesProgramadasPage extends StatefulWidget {
  @override
  _CapacitacionesProgramadasPageState createState() => _CapacitacionesProgramadasPageState();
}

class _CapacitacionesProgramadasPageState extends State<CapacitacionesProgramadasPage> {
  String name;

  @override
  void initState() {
    super.initState();
    SharedPreferences sharedPreferences = sL<SharedPreferences>();
    final jsonString = sharedPreferences.getString('CACHED_USER_DATA');
    UserDataModel userData = UserDataModel.fromJson(json.decode(jsonString));
    name = userData.nombres.split(' ')[0].capitalize() +
        " " +
        userData.apellidos.split(' ')[0].capitalize();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sL<CapacitacionesProgramadasBloc>()..add(GetCapacitacionesProgramadasEvent()),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Mis Capacitaciones Programadas", true),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(
          index: 2,
          name: name,
        ),
        body: BlocBuilder<CapacitacionesProgramadasBloc, CapacitacionesProgramadasState>(
          builder: (context, state) {
            if (state is Loading) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is Loaded) {
              ProximasCapacitaciones proximasCapacitaciones =
                  state.proximasCapacitaciones;
              return buildList(proximasCapacitaciones);
            } else {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Widget buildList(ProximasCapacitaciones proximasCapacitaciones) {
    var capacitaciones = proximasCapacitaciones.data;

    return ListView.separated(
      padding: EdgeInsets.all(16),
      itemCount: proximasCapacitaciones.count,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.5,
                blurRadius: 5,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  '/capacitacion',
                  arguments: capacitaciones[index],
                );
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        capacitaciones[index].enlace != null && capacitaciones[index].enlace.length > 0 ?
                        Icons.ondemand_video :
                        Icons.collections_bookmark,
                        color: Color.fromRGBO(209, 77, 56, 1.0),
                        size: 26,
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: _buildText(capacitaciones[index]),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          capacitaciones[index].tema,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14 / myTextScaleFactor,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(85, 85, 85, 1.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Fecha de capacitación",
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text(
                        DateFormat('dd/MM/yyyy').format(
                          capacitaciones[index].fechaIni,
                        ),
                        style: TextStyle(
                          fontSize: 13 / myTextScaleFactor,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                      /*Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "Vence en ",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Color.fromRGBO(85, 85, 85, 1.0),
                          ),
                        ),
                        Text(
                          "10 días",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(209, 77, 56, 1.0),
                          ),
                        ),
                      ],
                    ),
                  ),*/
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 16,
        );
      },
    );
  }

  List<Widget> _buildText(ProximaCapacitacion capacitacion) {
    String endDays = capacitacion.fechaFinal.difference(DateTime.now()).inDays.toString();
    String startDays = capacitacion.fechaIni.difference(DateTime.now()).inDays.toString();

    String text1;
    String text2;

    if (DateTime.now().compareTo(capacitacion.fechaIni) >= 0) {
      text1 = 'Finaliza en';
      text2 = ' $endDays días.';
    } else {
      text1 = 'Inicia en';
      text2 = ' $startDays días.';
    }

    List<Widget> items = new List<Widget>();

    items.add(Text(
      text1,
      style: TextStyle(
        fontSize: 13 / myTextScaleFactor,
        fontWeight: FontWeight.w300,
        color: Color.fromRGBO(85, 85, 85, 1.0),
      ),
    ),);
    items.add(Text(
      text2,
      style: TextStyle(
        fontSize: 13 / myTextScaleFactor,
        fontWeight: FontWeight.w500,
        color: Color.fromRGBO(209, 77, 56, 1.0),
      ),
    ),);
    return items;
  }
}