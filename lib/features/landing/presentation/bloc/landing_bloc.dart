import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/usecases/get_proximas_capacitaciones.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'landing_event.dart';
part 'landing_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class LandingBloc extends Bloc<LandingEvent, LandingState> {
  final GetCapacionesPorVencer getCapacionesPorVencer;
  final GetMisCapacitaciones getMisCapacitaciones;
  final GetProximasCapacitaciones getProximasCapacitaciones;

  LandingBloc({
    @required GetCapacionesPorVencer getCapacionesPorVencerCase,
    @required GetMisCapacitaciones getMisCapacitacionesCase,
    @required GetProximasCapacitaciones getProximasCapacitacionesCase,
  })  : assert(getCapacionesPorVencerCase != null),
        assert(getMisCapacitacionesCase != null),
        assert(getProximasCapacitacionesCase != null),
        getCapacionesPorVencer = getCapacionesPorVencerCase,
        getMisCapacitaciones = getMisCapacitacionesCase,
        getProximasCapacitaciones = getProximasCapacitacionesCase,
        super(Empty());

  @override
  Stream<LandingState> mapEventToState(
    LandingEvent event,
  ) async* {
    if (event is GetLandingDataEvent) {
      yield Loading();
      var future1 = getCapacionesPorVencer(NoParams());
      var future2 = getMisCapacitaciones(NoParams());
      var future3 = getProximasCapacitaciones(NoParams());

      final failureOrCapacitacionesPorVencer = await future1;
      final failureOrMisCapacitaciones = await future2;
      final failureOrProximasCapacitaciones = await future3;

      yield* failureOrCapacitacionesPorVencer.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (data1) async* {
          yield* failureOrMisCapacitaciones.fold(
            (failure) async* {
              yield Error(message: _mapFailureToMessage(failure));
            },
            (data2) async* {
              yield* failureOrProximasCapacitaciones.fold(
                (failure) async* {
                  yield Error(message: _mapFailureToMessage(failure));
                },
                (data3) async* {
                  yield Loaded(
                    capacitacionesPorVencer: data1,
                    misCapacitaciones: data2,
                    proximasCapacitaciones: data3,
                  );
                },
              );
            },
          );
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
