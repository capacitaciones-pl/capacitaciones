part of 'landing_bloc.dart';

abstract class LandingState extends Equatable {
  const LandingState();
}

class Empty extends LandingState {
  @override
  List<Object> get props => [];
}

class Loading extends LandingState{
  @override
  List<Object> get props => [];
}

class Loaded extends LandingState {
  final CapacitacionesPorVencer capacitacionesPorVencer;
  final MisCapacitaciones misCapacitaciones;
  final ProximasCapacitaciones proximasCapacitaciones;

  Loaded({
    @required this.capacitacionesPorVencer,
    @required this.misCapacitaciones,
    @required this.proximasCapacitaciones,
  });

  @override
  List<Object> get props => [
    capacitacionesPorVencer,
    misCapacitaciones,
    proximasCapacitaciones,
  ];
}

class Error extends LandingState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
