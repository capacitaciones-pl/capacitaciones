part of 'landing_bloc.dart';

abstract class LandingEvent extends Equatable {
  const LandingEvent();
}

class GetLandingDataEvent extends LandingEvent {
  @override
  List<Object> get props => [];
}
