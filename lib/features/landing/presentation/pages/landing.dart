import 'dart:convert';

import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/presentation/bloc/landing_bloc.dart';
import 'package:app_capacitaciones/features/landing/presentation/widgets/placeholder_loading.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';

import '../../../../injection_container.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  String name;
  int reload = 0;

  @override
  void initState() {
    super.initState();
    SharedPreferences sharedPreferences = sL<SharedPreferences>();
    final jsonString = sharedPreferences.getString('CACHED_USER_DATA');
    UserDataModel userData = UserDataModel.fromJson(json.decode(jsonString));
    name = userData.nombres.split(' ')[0].capitalize() +
        " " +
        userData.apellidos.split(' ')[0].capitalize();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sL<LandingBloc>()..add(GetLandingDataEvent()),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Hola " + name + "!", true),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(index: 0, name: name,),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                        padding: EdgeInsets.all(12),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: [
                              Color.fromRGBO(94, 129, 185, 1.0),
                              Color.fromRGBO(199, 194, 222, 1.0),
                              Color.fromRGBO(194, 147, 163, 1.0),
                            ],
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "John Wooden",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16 / myTextScaleFactor,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              "No dejes que lo que NO puedes hacer",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13 / myTextScaleFactor,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            Text(
                              "interfiera con lo que PUEDES hacer.",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13 / myTextScaleFactor,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            /*Row(
                              children: <Widget>[
                                Text(
                                  "No dejes que lo que NO puedes hacer",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                Text(
                                  "45%",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Stack(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                      height: 10,
                                      width: 150,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(12, 2, 0, 0),
                                      height: 6,
                                      width: 146 * (45 / 100),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Color.fromRGBO(209, 77, 56, 1.0),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),*/
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 16, 16),
                          child: Image.asset(
                            "assets/images/books.png",
                            height: 80,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Mis capacitaciones programadas",
                        style: TextStyle(
                          color: Color.fromRGBO(100, 133, 16, 1.0),
                          fontSize: 16 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/capacitaciones_programadas');
                              },
                              child: Text(
                                "ver más",
                                style: TextStyle(
                                  color: Color.fromRGBO(209, 77, 56, 1.0),
                                  fontSize: 14 / myTextScaleFactor,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 108,
                  child: buildCapacitacionesPorVencer(),
                ),
                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Mis capacitaciones",
                        style: TextStyle(
                          color: Color.fromRGBO(100, 133, 16, 1.0),
                          fontSize: 16 / myTextScaleFactor,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/mis_capacitaciones');
                              },
                              child: Text(
                                "ver más",
                                style: TextStyle(
                                  color: Color.fromRGBO(209, 77, 56, 1.0),
                                  fontSize: 14 / myTextScaleFactor,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 108,
                  child: buildMisCapacitaciones(),
                ),
              ],
            ),
            DraggableScrollableSheet(
              initialChildSize: 0.25,
              minChildSize: 0.25,
              maxChildSize: 0.8,
              builder: (BuildContext context, scrollController) {
                return Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(37, 76, 113, 1.0),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 54,
                        child: ListView(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          controller: scrollController,
                          children: <Widget>[
                            Center(
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(255, 255, 255, 1.0),
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                height: 4,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Próximas Capacitaciones",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16 / myTextScaleFactor,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Icon(
                                  Icons.event,
                                  size: 24,
                                  color: Colors.white,
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.pushNamed(context, '/proximas_capacitaciones');
                                        },
                                        child: Text(
                                          "ver más",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14 / myTextScaleFactor,
                                            fontWeight: FontWeight.w300,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: buildProximasCapacitaciones(scrollController),
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCapacitacionesPorVencer() {
    return BlocBuilder<LandingBloc, LandingState>(
      builder: (context, state) {
        if (state is Loading) {
          return LoadingPlaceholder();
          /*return Center(
            child: CircularProgressIndicator(),
          );*/
        } else if (state is Loaded) {
          return ListView.separated(
            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: state.proximasCapacitaciones.data.length < 5 ? state.proximasCapacitaciones.data.length : 5,
            itemBuilder: (BuildContext context, int index) {
              return Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/capacitacion',
                      arguments: state.proximasCapacitaciones.data[index],
                    );
                  },
                  child: Container(
                    width: 250.0,
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 0.5,
                          blurRadius: 5,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(
                              state.proximasCapacitaciones.data[index].enlace != null && state.proximasCapacitaciones.data[index].enlace.length > 0 ?
                              Icons.ondemand_video :
                              Icons.collections_bookmark,
                              color: Color.fromRGBO(209, 77, 56, 1.0),
                              size: 26,
                            ),
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Color.fromRGBO(100, 133, 16, 1.0),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(2, 2, 0, 0),
                          child: Text(
                            state.proximasCapacitaciones.data[index].tema,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 14 / myTextScaleFactor,
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(85, 85, 85, 1.0),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(2, 2, 0, 0),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Fecha de capacitación",
                                style: TextStyle(
                                  fontSize: 13 / myTextScaleFactor,
                                  fontWeight: FontWeight.w300,
                                  color: Color.fromRGBO(85, 85, 85, 1.0),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    DateFormat('dd/MM/yyyy').format(state
                                        .proximasCapacitaciones
                                        .data[index]
                                        .fechaIni),
                                    style: TextStyle(
                                      fontSize: 13 / myTextScaleFactor,
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(85, 85, 85, 1.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                width: 16,
              );
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget buildMisCapacitaciones() {
    return BlocBuilder<LandingBloc, LandingState>(
      builder: (context, state) {
        if (state is Loading) {
          return LoadingPlaceholder();
        } else if (state is Loaded) {
          final List<MiCapacitacion> misCapacitaciones =
              state.misCapacitaciones.data;
          return ListView.separated(
            // This next line does the trick.
            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: misCapacitaciones.length < 5 ? misCapacitaciones.length : 5,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                width: 250.0,
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 0.5,
                      blurRadius: 5,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.collections_bookmark,
                          size: 26,
                          color: Color.fromRGBO(209, 77, 56, 1.0),
                        ),
                        Expanded(
                          child: _buildRowNota(misCapacitaciones[index].nota, misCapacitaciones[index].estado),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(2, 2, 0, 0),
                      child: Text(
                        misCapacitaciones[index].tema,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 14 / myTextScaleFactor,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(85, 85, 85, 1.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(2, 2, 0, 0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Fecha de capacitación",
                            style: TextStyle(
                              fontSize: 13 / myTextScaleFactor,
                              fontWeight: FontWeight.w300,
                              color: Color.fromRGBO(85, 85, 85, 1.0),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                DateFormat('dd/MM/yyyy')
                                    .format(misCapacitaciones[index].fechaIni),
                                style: TextStyle(
                                  fontSize: 13 / myTextScaleFactor,
                                  fontWeight: FontWeight.w400,
                                  color: Color.fromRGBO(85, 85, 85, 1.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                width: 16,
              );
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget buildProximasCapacitaciones(ScrollController scrollController) {
    return BlocBuilder<LandingBloc, LandingState>(
      builder: (context, state) {
        if (state is Loading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is Loaded) {
          //Intl.defaultLocale = 'es';
          //initializeDateFormatting('es', null);
          final List<CapacitacionPorVencer> proximasCapacitaciones =
              state.capacitacionesPorVencer.data;
          return ListView.separated(
            padding: EdgeInsets.all(16),
            itemCount: proximasCapacitaciones.length < 15 ? proximasCapacitaciones.length : 15,
            controller: scrollController,
            itemBuilder: (BuildContext context, int index) {
              return Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/capacitacion_ins',
                      arguments: state.capacitacionesPorVencer.data[index],
                    ).then(
                      (value) {
                        BlocProvider.of<LandingBloc>(context).add(GetLandingDataEvent());
                      },
                    );
                  },
                  /*onTap: () {
                    Navigator.pushNamed(context, '/capacitacion');
                  },*/
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(209, 77, 56, 1.0),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Icon(
                          proximasCapacitaciones[index].enlace != null && proximasCapacitaciones[index].enlace.length > 0 ?
                          Icons.ondemand_video :
                          Icons.collections_bookmark,
                          color: Colors.white,
                          size: 26,
                        ),
                        padding: EdgeInsets.all(4),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              proximasCapacitaciones[index].tema,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14 / myTextScaleFactor,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              //"20 de Julio de 2020 . 10:00 a.m.",
                              DateFormat("dd 'de' MMMM 'de' yyyy . hh:mm a")
                                  .format(proximasCapacitaciones[index].fechaIni),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13 / myTextScaleFactor,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.more_vert,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 18,
              );
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
  
  Widget _buildRowNota(double nota, String estado) {
    String notaStr = '';
    String notaS = '';
    
    if (nota == null) {
      notaStr = estado;
      notaS = '-';
    } else {
      notaStr = estado;
      notaS = nota.round().toString();
    }
    
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          notaStr,
          style: TextStyle(
            fontSize: 12 / myTextScaleFactor,
            fontWeight: FontWeight.w300,
            color: Color.fromRGBO(85, 85, 85, 1.0),
          ),
        ),
        SizedBox(
          width: 8,
        ),
        Text(
          notaS,
          style: TextStyle(
            fontSize: 18 / myTextScaleFactor,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(100, 133, 16, 1.0),
          ),
        ),
      ],
    );
  }
}
