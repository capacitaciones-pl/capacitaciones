import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class CapacitacionesPorVencer extends Equatable {
  final int count;
  final List<CapacitacionPorVencer> data;

  CapacitacionesPorVencer({
    @required this.count,
    @required this.data,
  });

  @override
  List<Object> get props => [count, data];
}

class CapacitacionPorVencer extends Equatable {
  final String codCurso;
  final String tema;
  final DateTime fechaIni;
  final DateTime fechaFinal;
  final DateTime fechaVen;
  final int vigencia;
  final String vigenciatxt;
  final String codVigenciaCapacita;
  final String duracion;
  final String horaInicio;
  final String tipo;
  final double puntajeTotal;
  final int capacidad;
  final String codEmpresaCap;
  final String empresaCap;
  final String codTemaCapacita;
  final bool inscrito;
  final int disponibles;
  final String enlace;
  final List<Expositor> expositores;

  CapacitacionPorVencer({
    @required this.codCurso,
    @required this.tema,
    @required this.fechaIni,
    @required this.fechaFinal,
    @required this.fechaVen,
    @required this.vigencia,
    @required this.vigenciatxt,
    @required this.codVigenciaCapacita,
    @required this.duracion,
    @required this.horaInicio,
    @required this.tipo,
    @required this.puntajeTotal,
    @required this.capacidad,
    @required this.codEmpresaCap,
    @required this.empresaCap,
    @required this.codTemaCapacita,
    @required this.inscrito,
    @required this.disponibles,
    @required this.enlace,
    @required this.expositores,
  });

  @override
  List<Object> get props => [
    codCurso,
    tema,
    fechaIni,
    fechaFinal,
    fechaVen,
    vigencia,
    vigenciatxt,
    codVigenciaCapacita,
    duracion,
    horaInicio,
    tipo,
    puntajeTotal,
    capacidad,
    codEmpresaCap,
    empresaCap,
    codTemaCapacita,
    inscrito,
    disponibles,
    enlace,
    expositores
  ];
}

class Expositor extends Equatable {
  final String codPersona;
  final String codTemaCapacita;
  final bool tipo;
  final String nombre;

  Expositor({
    @required this.codPersona,
    @required this.codTemaCapacita,
    @required this.tipo,
    @required this.nombre,
  });

  @override
  List<Object> get props => [
    codPersona,
    codTemaCapacita,
    tipo,
    nombre
  ];
}

//"codCurso": "00000100",
//"tema": "curso 1",
//"fechaIni": "2020-08-29T12:00:00",
//"fechaFinal": "2020-08-29T14:00:00",
//"fechaVen": "2021-01-29T12:00:00",
//"vigencia": 5,
//"codVigenciaCapacita": "3",
//"duracion": "5 meses",
//"vencimiento": true,
//"codTemaCapacita": "CE001"
