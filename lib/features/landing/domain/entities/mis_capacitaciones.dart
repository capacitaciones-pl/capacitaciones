import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class MisCapacitaciones extends Equatable {
  final int count;
  final List<MiCapacitacion> data;

  MisCapacitaciones({
    this.count,
    this.data,
  });

  @override
  List<Object> get props => [
    count,
    data,
  ];
}

class MiCapacitacion extends Equatable {
  final String codTemaCapacita;
  final String codCurso;
  final String tema;
  final DateTime fechaIni;
  final DateTime fechaFinal;
  final DateTime fechaVen;
  final int vigencia;
  final String codVigenciaCapacita;
  final double nota;
  final String estado;
  final bool certificado;

  MiCapacitacion({
    @required this.codTemaCapacita,
    @required this.codCurso,
    @required this.tema,
    @required this.fechaIni,
    @required this.fechaFinal,
    @required this.fechaVen,
    @required this.vigencia,
    @required this.codVigenciaCapacita,
    @required this.nota,
    @required this.estado,
    @required this.certificado,
});

  @override
  List<Object> get props => [
    codTemaCapacita,
    codCurso,
    tema,
    fechaIni,
    fechaFinal,
    fechaVen,
    vigencia,
    codVigenciaCapacita,
    nota,
    estado,
    certificado,
  ];
}

//"codTemaCapacita": "CE001",
//"codCurso": "00000100",
//"tema": "curso 1",
//"fechaIni": "2020-08-29T12:00:00",
//"fechaFinal": "2020-08-29T14:00:00",
//"fechaVen": "2021-01-29T12:00:00",
//"vigencia": 5,
//"codVigenciaCapacita": "3",
//"nota": 12.50,
//"estado": true