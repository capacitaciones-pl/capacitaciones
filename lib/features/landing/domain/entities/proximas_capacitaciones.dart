import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ProximasCapacitaciones extends Equatable {
  final int count;
  final List<ProximaCapacitacion> data;

  ProximasCapacitaciones({
    @required this.count,
    @required this.data,
  });

  @override
  List<Object> get props => [count, data];
}

class ProximaCapacitacion extends Equatable {
  final String codCurso;
  final String tema;
  final DateTime fechaIni;
  final DateTime fechaFinal;
  final DateTime fechaVen;
  final int vigencia;
  final String vigenciatxt;
  final String codVigenciaCapacita;
  final String duracion;
  final String horaFin;
  final bool estadoCurso;
  final String codTemaCapacita;
  final bool vencimiento;
  final String horaInicio;
  final String tipo;
  final double puntajeTotal;
  final int capacidad;
  final String codEmpresaCap;
  final String empresaCap;
  final String enlace;
  final int intentos;
  final bool inscrito;
  final int disponibles;
  final List<Expositor> expositores;

  ProximaCapacitacion({
    @required this.codCurso,
    @required this.tema,
    @required this.fechaIni,
    @required this.fechaFinal,
    @required this.fechaVen,
    @required this.vigencia,
    @required this.vigenciatxt,
    @required this.codVigenciaCapacita,
    @required this.duracion,
    @required this.horaFin,
    @required this.estadoCurso,
    @required this.codTemaCapacita,
    @required this.vencimiento,
    @required this.horaInicio,
    @required this.tipo,
    @required this.puntajeTotal,
    @required this.capacidad,
    @required this.codEmpresaCap,
    @required this.empresaCap,
    @required this.enlace,
    @required this.intentos,
    @required this.inscrito,
    @required this.disponibles,
    @required this.expositores,
  });

  @override
  List<Object> get props => [
    codCurso,
    tema,
    fechaIni,
    fechaFinal,
    fechaVen,
    vigencia,
    vigenciatxt,
    codVigenciaCapacita,
    duracion,
    horaFin,
    estadoCurso,
    codTemaCapacita,
    vencimiento,
    horaInicio,
    tipo,
    puntajeTotal,
    capacidad,
    codEmpresaCap,
    empresaCap,
    enlace,
    intentos,
    inscrito,
    disponibles,
    expositores,
  ];
}

class Expositor extends Equatable {
  final String codPersona;
  final String codTemaCapacita;
  final bool tipo;
  final String nombre;

  Expositor({
    @required this.codPersona,
    @required this.codTemaCapacita,
    @required this.tipo,
    @required this.nombre,
  });

  @override
  List<Object> get props => [
    codPersona,
    codTemaCapacita,
    tipo,
    nombre
  ];
}


//"codCurso": "00000100",
//"tema": "curso 1",
//"fechIni": "2020-08-29T12:00:00",
//"fechaFinal": "2020-08-29T14:00:00",
//"fechaVen": "2021-01-29T12:00:00",
//"vigencia": 5,
//"codVigenciaCapacita": "3",
//"duracion": "5 meses",
//"horaFin": "02:00 PM",
//"estadoCurso": false,
//"codTemaCapacita": "CE001",
//"vencimiento": true,
//"horaInicio": "12:00 PM",
//"tipo": "01",
//"puntajeTotal": 20.00,
//"capacidad": 50,
//"codEmpresaCap": "2",
//"codParticipante": null,
//"inscrito": true,
//"disponibles": 49,
//"expositores": [
//{
//"codPersona": "0043316109",
//"codTemaCapacita": "00000100",
//"tipo": true,
//"nombre": "CCAPATINTA CHINO CLAUDIA MATILDE"
//}
//]