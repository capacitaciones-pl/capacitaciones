import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/capacitaciones_vencer_repository.dart';
import 'package:dartz/dartz.dart';

class GetCapacionesPorVencer implements UseCase<CapacitacionesPorVencer, NoParams> {
  final CapacitacionesPorVencerRepository repository;

  GetCapacionesPorVencer(this.repository);

  @override
  Future<Either<Failure, CapacitacionesPorVencer>> call(NoParams params) async {
    return await repository.getCapacitacionesPorVencer();
  }
}