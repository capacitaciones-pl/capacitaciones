import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/mis_capacitaciones_repository.dart';
import 'package:dartz/dartz.dart';

class GetMisCapacitaciones implements UseCase<MisCapacitaciones, NoParams> {
  final MisCapacitacionesRepository repository;

  GetMisCapacitaciones(this.repository);

  @override
  Future<Either<Failure, MisCapacitaciones>> call(NoParams params) async {
    return await repository.getMisCapacitaciones();
  }
}