import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/proximas_capacitaciones_repository.dart';
import 'package:dartz/dartz.dart';

class GetProximasCapacitaciones implements UseCase<ProximasCapacitaciones, NoParams> {
  final ProximasCapacitacionesRepository repository;

  GetProximasCapacitaciones(this.repository);

  @override
  Future<Either<Failure, ProximasCapacitaciones>> call(NoParams params) async {
    return await repository.getProximasCapacitaciones();
  }
}