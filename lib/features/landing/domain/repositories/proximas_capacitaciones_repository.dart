import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:dartz/dartz.dart';

abstract class ProximasCapacitacionesRepository {
  Future<Either<Failure, ProximasCapacitaciones>> getProximasCapacitaciones();
}