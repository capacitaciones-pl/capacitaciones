import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:dartz/dartz.dart';

abstract class MisCapacitacionesRepository {
  Future<Either<Failure, MisCapacitaciones>> getMisCapacitaciones();
}