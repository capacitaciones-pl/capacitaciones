import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:dartz/dartz.dart';

abstract class CapacitacionesPorVencerRepository {
  Future<Either<Failure, CapacitacionesPorVencer>> getCapacitacionesPorVencer();
}
