import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/landing/data/datasources/proximas_capacitaciones_data_source.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/proximas_capacitaciones_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class ProximasCapacitacionesRepositoryImpl
    implements ProximasCapacitacionesRepository {

  final ProximasCapacitacionesDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  ProximasCapacitacionesRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, ProximasCapacitaciones>>
      getProximasCapacitaciones() async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final remoteUserData = await remoteDataSource.getProximasCapacitaciones(
            localUserData.codPersona, localUserData.token);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}
