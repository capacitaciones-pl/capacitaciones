import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/landing/data/datasources/capacitaciones_vencer_data_source.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:app_capacitaciones/features/landing/domain/repositories/capacitaciones_vencer_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class CapacitacionesPorVencerRepositoryImpl
    implements CapacitacionesPorVencerRepository {

  final CapacitacionesPorVencerDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  CapacitacionesPorVencerRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, CapacitacionesPorVencer>>
  getCapacitacionesPorVencer() async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final remoteUserData = await remoteDataSource.getCapacitacionesPorVencer(
            localUserData.codPersona, localUserData.token);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}