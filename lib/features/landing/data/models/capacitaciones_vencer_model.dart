import 'package:app_capacitaciones/features/landing/domain/entities/capacitaciones_vencer.dart';
import 'package:meta/meta.dart';

class CapacitacionesPorVencerModel extends CapacitacionesPorVencer {
  CapacitacionesPorVencerModel({
    @required int count,
    @required List<CapacitacionPorVencerModel> data,
  }) : super(
    count: count,
    data: data,
  );

  factory CapacitacionesPorVencerModel.fromJson(Map<String, dynamic> json) {
    return CapacitacionesPorVencerModel(
      count: json['count'],
      data: (json['data'] as List).map((e) => CapacitacionPorVencerModel.fromJson(e)).toList(),
    );
  }
}

class CapacitacionPorVencerModel extends CapacitacionPorVencer {

  CapacitacionPorVencerModel({
    @required String codCurso,
    @required String tema,
    @required DateTime fechaIni,
    @required DateTime fechaFinal,
    @required DateTime fechaVen,
    @required int vigencia,
    @required String vigenciatxt,
    @required String codVigenciaCapacita,
    @required String duracion,
    @required String horaInicio,
    @required String tipo,
    @required double puntajeTotal,
    @required int capacidad,
    @required String codEmpresaCap,
    @required String empresaCap,
    @required String codTemaCapacita,
    @required bool inscrito,
    @required int disponibles,
    @required String enlace,
    @required List<Expositor> expositores,
  }) : super(
          codCurso: codCurso,
          tema: tema,
          fechaIni: fechaIni,
          fechaFinal: fechaFinal,
          fechaVen: fechaVen,
          vigencia: vigencia,
          vigenciatxt: vigenciatxt,
          codVigenciaCapacita: codVigenciaCapacita,
          duracion: duracion,
          horaInicio: horaInicio,
          tipo: tipo,
          puntajeTotal: puntajeTotal,
          capacidad: capacidad,
          codEmpresaCap: codEmpresaCap,
          empresaCap: empresaCap,
          codTemaCapacita: codTemaCapacita,
          inscrito: inscrito,
          disponibles: disponibles,
          enlace: enlace,
          expositores: expositores,
        );

  factory CapacitacionPorVencerModel.fromJson(Map<String, dynamic> json) {
    return CapacitacionPorVencerModel(
      codCurso: json['codCurso'],
      tema: json['tema'],
      fechaIni: json['fechaIni'] == null ? null : DateTime.parse(json['fechaIni']),
      fechaFinal: json['fechaFinal'] == null ? null : DateTime.parse(json['fechaFinal']),
      fechaVen: json['fechaVen'] == null ? null : DateTime.parse(json['fechaVen']),
      vigencia: json['vigencia'],
      vigenciatxt: json['vigenciatxt'],
      codVigenciaCapacita: json['codVigenciaCapacita'],
      duracion: json['duracion'],
      horaInicio: json['horaInicio'],
      tipo: json['tipo'],
      puntajeTotal: json['puntajeTotal'],
      capacidad: json['capacidad'],
      codEmpresaCap: json['codEmpresaCap'],
      empresaCap: json['empresaCap'],
      codTemaCapacita: json['codTemaCapacita'],
      inscrito: json['inscrito'],
      disponibles: json['disponibles'],
      enlace: json['enlace'],
      expositores: (json['expositores'] as List).map((e) => ExpositorModel.fromJson(e)).toList(),
    );
  }
}

class ExpositorModel extends Expositor {
  ExpositorModel({
    @required String codPersona,
    @required String codTemaCapacita,
    @required bool tipo,
    @required String nombre,
  }) : super(
    codPersona : codPersona,
    codTemaCapacita: codTemaCapacita,
    tipo: tipo,
    nombre: nombre,
  );

  factory ExpositorModel.fromJson(Map<String, dynamic> json) {
    return ExpositorModel(
      codPersona: json['codPersona'],
      codTemaCapacita: json['codTemaCapacita'],
      tipo: json['tipo'],
      nombre: json['nombre'],
    );
  }
}