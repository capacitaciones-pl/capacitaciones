import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:meta/meta.dart';

class ProximasCapacitacionesModel extends ProximasCapacitaciones {
  ProximasCapacitacionesModel({
    @required int count,
    @required List<ProximaCapacitacionModel> data,
  }) : super(
    count: count,
    data: data,
  );

  factory ProximasCapacitacionesModel.fromJson(Map<String, dynamic> json) {
    return ProximasCapacitacionesModel(
      count: json['count'],
      data: (json['data'] as List).map((e) => ProximaCapacitacionModel.fromJson(e)).toList(),
    );
  }
}

class ProximaCapacitacionModel extends ProximaCapacitacion {
  ProximaCapacitacionModel({
    @required String codCurso,
    @required String tema,
    @required DateTime fechaIni,
    @required DateTime fechaFinal,
    @required DateTime fechaVen,
    @required int vigencia,
    @required String vigenciatxt,
    @required String codVigenciaCapacita,
    @required String duracion,
    @required String horaFin,
    @required bool estadoCurso,
    @required String codTemaCapacita,
    @required bool vencimiento,
    @required String horaInicio,
    @required String tipo,
    @required double puntajeTotal,
    @required int capacidad,
    @required String codEmpresaCap,
    @required String empresaCap,
    @required String enlace,
    @required int intentos,
    @required bool inscrito,
    @required int disponibles,
    @required List<Expositor> expositores,
  }) : super(
    codCurso: codCurso,
    tema: tema,
    fechaIni: fechaIni,
    fechaFinal: fechaFinal,
    fechaVen: fechaVen,
    vigencia: vigencia,
    vigenciatxt: vigenciatxt,
    codVigenciaCapacita: codVigenciaCapacita,
    duracion: duracion,
    horaFin: horaFin,
    estadoCurso: estadoCurso,
    codTemaCapacita: codTemaCapacita,
    vencimiento: vencimiento,
    horaInicio: horaInicio,
    tipo: tipo,
    puntajeTotal: puntajeTotal,
    capacidad: capacidad,
    codEmpresaCap: codEmpresaCap,
    empresaCap: empresaCap,
    enlace: enlace,
    intentos: intentos,
    inscrito: inscrito,
    disponibles: disponibles,
    expositores: expositores,
  );

  factory ProximaCapacitacionModel.fromJson(Map<String, dynamic> json) {
    return ProximaCapacitacionModel(
      codCurso: json['codCurso'],
      tema: json['tema'],
      fechaIni: json['fechIni'] == null ? null : DateTime.parse(json['fechIni']),
      fechaFinal: json['fechaFinal'] == null ? null : DateTime.parse(json['fechaFinal']),
      fechaVen: json['fechaVen'] == null ? null : DateTime.parse(json['fechaVen']),
      vigencia: json['vigencia'],
      vigenciatxt: json['vigenciatxt'],
      codVigenciaCapacita: json['codVigenciaCapacita'],
      duracion: json['duracion'],
      horaFin: json['horaFin'],
      estadoCurso: json['estadoCurso'],
      codTemaCapacita: json['codTemaCapacita'],
      vencimiento: json['vencimiento'],
      horaInicio: json['horaInicio'],
      tipo: json['tipo'],
      puntajeTotal: json['puntajeTotal'],
      capacidad: json['capacidad'],
      codEmpresaCap: json['codEmpresaCap'],
      empresaCap: json['empresaCap'],
      enlace: json['enlace'],
      intentos: json['intentos'],
      inscrito: json['inscrito'],
      disponibles: json['disponibles'],
      expositores: (json['expositores'] as List).map((e) => ExpositorModel.fromJson(e)).toList(),
    );
  }
}

class ExpositorModel extends Expositor {
  ExpositorModel({
    @required String codPersona,
    @required String codTemaCapacita,
    @required bool tipo,
    @required String nombre,
  }) : super(
    codPersona : codPersona,
    codTemaCapacita: codTemaCapacita,
    tipo: tipo,
    nombre: nombre,
  );

  factory ExpositorModel.fromJson(Map<String, dynamic> json) {
    return ExpositorModel(
      codPersona: json['codPersona'],
      codTemaCapacita: json['codTemaCapacita'],
      tipo: json['tipo'],
      nombre: json['nombre'],
    );
  }
}