import 'package:app_capacitaciones/features/landing/domain/entities/mis_capacitaciones.dart';
import 'package:meta/meta.dart';

class MisCapacitacionesModel extends MisCapacitaciones {
  MisCapacitacionesModel({
    @required int count,
    @required List<MiCapacitacionModel> data,
  }) : super(
    count: count,
    data: data,
  );

  factory MisCapacitacionesModel.fromJson(Map<String, dynamic> json) {
    return MisCapacitacionesModel(
      count: json['count'],
      data: (json['data'] as List).map((e) => MiCapacitacionModel.fromJson(e)).toList(),
    );
  }
}

class MiCapacitacionModel extends MiCapacitacion {
  MiCapacitacionModel({
    @required String codTemaCapacita,
    @required String codCurso,
    @required String tema,
    @required DateTime fechaIni,
    @required DateTime fechaFinal,
    @required DateTime fechaVen,
    @required int vigencia,
    @required String codVigenciaCapacita,
    @required double nota,
    @required String estado,
    @required bool certificado,
  }) : super (
    codTemaCapacita: codTemaCapacita,
    codCurso: codCurso,
    tema: tema,
    fechaIni: fechaIni,
    fechaFinal: fechaFinal,
    fechaVen: fechaVen,
    vigencia: vigencia,
    codVigenciaCapacita: codVigenciaCapacita,
    nota: nota,
    estado: estado,
    certificado: certificado
  );

  factory MiCapacitacionModel.fromJson(Map<String, dynamic> json) {
    return MiCapacitacionModel(
      codTemaCapacita: json['codTemaCapacita'],
      codCurso: json['codCurso'],
      tema: json['tema'],
      fechaIni: json['fechaIni'] == null ? null : DateTime.parse(json['fechaIni']),
      fechaFinal: json['fechaFinal'] == null ? null : DateTime.parse(json['fechaFinal']),
      fechaVen: json['fechaVen'] == null ? null : DateTime.parse(json['fechaVen']),
      vigencia: json['vigencia'],
      codVigenciaCapacita: json['codVigenciaCapacita'],
      nota: json['nota'],
      estado: json['estado'],
      certificado: json['certificado'],
    );
  }
}