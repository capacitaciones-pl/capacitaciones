import 'dart:convert';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/landing/data/models/proximas_capacitaciones_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class ProximasCapacitacionesDataSource {
  Future<ProximasCapacitacionesModel> getProximasCapacitaciones(String codPersona, String token);
}

class ProximasCapacitacionesDataSourceImpl implements ProximasCapacitacionesDataSource {
  final http.Client client;

  ProximasCapacitacionesDataSourceImpl({
    @required this.client,
  });

  @override
  Future<ProximasCapacitacionesModel> getProximasCapacitaciones(String codPersona, String token) async {
    final response = await client.get(
      '$MHSEC_CAP/capacitaciones/getmiscursosinscritos/getestadocursos/$codPersona',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return ProximasCapacitacionesModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}