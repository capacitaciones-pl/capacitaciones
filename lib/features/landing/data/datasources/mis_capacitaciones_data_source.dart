import 'dart:convert';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/landing/data/models/mis_capacitaciones_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class MisCapacitacionesDataSource {
  Future<MisCapacitacionesModel> getMisCapacitaciones(String codPersona, String token);
}

class MisCapacitacionesDataSourceImpl implements MisCapacitacionesDataSource {
  final http.Client client;

  MisCapacitacionesDataSourceImpl({
    @required this.client,
  });

  @override
  Future<MisCapacitacionesModel> getMisCapacitaciones(String codPersona, String token) async {
    final response = await client.get(
      '$MHSEC_CAP/capacitaciones/getmiscapacitaciones/getestadocursos/$codPersona',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    /*await Future.delayed(
      Duration(
        seconds: 2,
      ),
    );*/

    if (response.statusCode == 200) {
      return MisCapacitacionesModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}