import 'dart:convert';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/landing/data/models/capacitaciones_vencer_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class CapacitacionesPorVencerDataSource {
  Future<CapacitacionesPorVencerModel> getCapacitacionesPorVencer(String codPersona, String token);
}

class CapacitacionesPorVencerDataSourceImpl implements CapacitacionesPorVencerDataSource {
  final http.Client client;

  CapacitacionesPorVencerDataSourceImpl({
    @required this.client,
  });

  @override
  Future<CapacitacionesPorVencerModel> getCapacitacionesPorVencer(String codPersona, String token) async {
    final response = await client.get(
      '$MHSEC_CAP/capacitaciones/getmiscursosdisponibles/getestadocursos/sinfiltro/1/100',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return CapacitacionesPorVencerModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}