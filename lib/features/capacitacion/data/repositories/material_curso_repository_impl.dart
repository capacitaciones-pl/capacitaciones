import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/capacitacion/data/datasources/materiales_curso_data_source.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/request_update.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/repositories/material_curso_repository.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class MaterialCursoRepositoryImpl implements MaterialCursoRepository {
  final MaterialesCursoDataSource remoteDataSource;
  final UserDataLocalDataSource userDataLocalDataSource;
  final NetworkInfo networkInfo;

  MaterialCursoRepositoryImpl({
    @required this.remoteDataSource,
    @required this.userDataLocalDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, MaterialesCurso>> cargarMateriales(String codCurso) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();
        final data = await remoteDataSource.getMaterialesCurso(
            localUserData.codPersona,
            codCurso, localUserData.token);

        bool enable = data.data.isNotEmpty;
        data.data.forEach((element) {
          if (element.revisado != 2) {
            enable = false;
            return;
          }
        });

        data.enableButton = enable;

        return Right(data);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }

  @override
  Future<Either<Failure, MaterialesCurso>> actualizarMaterial(String codCurso, int codArchivo) async {
    if (await networkInfo.isConnected) {
      try {
        final localUserData = await userDataLocalDataSource.getLastUserData();

        final data = await remoteDataSource.updateMaterialCurso(
          RequestUpdate(
            codCurso: codCurso,
            codPersona: localUserData.codPersona,
            codArchivo: codArchivo,
          ),
          localUserData.token,
        );

        bool enable = data.data.isNotEmpty;
        data.data.forEach((element) {
          if (element.revisado != 2) {
            enable = false;
            return;
          }
        });

        data.enableButton = enable;

        return Right(data);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(NoInternetFailure());
    }
  }
}