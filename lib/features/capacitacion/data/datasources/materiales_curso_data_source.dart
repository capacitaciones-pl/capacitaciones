import 'dart:convert';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/capacitacion/data/models/materiales_curso_model.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/request_update.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class MaterialesCursoDataSource {
  Future<MaterialesCursoModel> getMaterialesCurso(String codPersona, String codCurso, String token);
  Future<MaterialesCursoModel> updateMaterialCurso(RequestUpdate request, String token);
}

class MaterialesCursoDataSourceImpl implements MaterialesCursoDataSource {
  final http.Client client;

  MaterialesCursoDataSourceImpl({
    @required this.client,
  });

  @override
  Future<MaterialesCursoModel> getMaterialesCurso(String codPersona, String codCurso, String token) async {
    final response = await client.get(
      '$MHSEC_PLAN/accion/file/get/coddocreferencia?codref=$codCurso&codsub=$codPersona&codtabla=TCAP',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      return MaterialesCursoModel.fromJson(json.decode(response.body), codPersona);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<MaterialesCursoModel> updateMaterialCurso(RequestUpdate request, String token) async {
    final response = await client.post(
      '$MHSEC_PLAN/accion/file/updatevalfilesrevisado',
      body: json.encode(request.toJson()),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      var codCurso = request.codCurso;
      var codPersona = request.codPersona;
      final response1 = await client.get(
        '$MHSEC_PLAN/accion/file/get/coddocreferencia?codref=$codCurso&codsub=$codPersona&codtabla=TCAP',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response1.statusCode == 200) {
        return MaterialesCursoModel.fromJson(json.decode(response1.body), codPersona);
      } else {
        throw ServerException();
      }
    } else {
      throw ServerException();
    }
  }
}