import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:meta/meta.dart';

class MaterialesCursoModel extends MaterialesCurso {
  MaterialesCursoModel({
    @required int count,
    @required List<MaterialCurso> data,
  }) : super(
          count: count,
          data: data,
        );

  factory MaterialesCursoModel.fromJson(Map<String, dynamic> json, String codPersona) {
    return MaterialesCursoModel(
      count: json['count'],
      data: (json['data'] as List).map((e) => MaterialCursoModel.fromJson(e, codPersona)).toList(),
    );
  }
}

class MaterialCursoModel extends MaterialCurso {
  MaterialCursoModel({
    @required int correlativoArchivos,
    @required String nroDocReferencia,
    @required String nroSubDocReferencia,
    @required String codTablaRef,
    @required int grupoPertenece,
    @required String descripcion,
    @required String tipoArchivo,
    @required String nombre,
    @required int size,
    @required bool estado,
    @required int revisado,
    @required String codPersona,
  }) : super (
    codPersona: codPersona,
    correlativoArchivos: correlativoArchivos,
    nroDocReferencia: nroDocReferencia,
    nroSubDocReferencia: nroSubDocReferencia,
    codTablaRef: codTablaRef,
    grupoPertenece: grupoPertenece,
    descripcion: descripcion,
    tipoArchivo: tipoArchivo,
    nombre: nombre,
    size: size,
    estado: estado,
    revisado: revisado,
  );

  factory MaterialCursoModel.fromJson(Map<String, dynamic> json, String codPersona) {
    return MaterialCursoModel(
      correlativoArchivos: json['correlativoArchivos'],
      nroDocReferencia: json['nroDocReferencia'],
      nroSubDocReferencia: json['nroSubDocReferencia'],
      codTablaRef: json['codTablaRef'],
      grupoPertenece: json['grupoPertenece'],
      descripcion: json['descripcion'],
      tipoArchivo: json['tipoArchivo'],
      nombre: json['nombre'],
      size: json['size'],
      estado: json['estado'],
      revisado: json['revisado'],
      codPersona: codPersona,
    );
  }
}