import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/repositories/material_curso_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ActualizarMaterial implements UseCase<MaterialesCurso, ParamsA> {
  final MaterialCursoRepository repository;

  ActualizarMaterial(this.repository);

  @override
  Future<Either<Failure, MaterialesCurso>> call(ParamsA params) async {
    return await repository.actualizarMaterial(params.codCurso, params.codArchivo);
  }
}

class ParamsA extends Equatable {
  final String codCurso;
  final int codArchivo;

  ParamsA({
    @required this.codCurso,
    @required this.codArchivo
  });

  @override
  List<Object> get props => [codCurso, codArchivo];
}