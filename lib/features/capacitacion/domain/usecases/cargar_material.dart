import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/repositories/material_curso_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class CargarMaterial implements UseCase<MaterialesCurso, Params> {
  final MaterialCursoRepository repository;

  CargarMaterial(this.repository);

  @override
  Future<Either<Failure, MaterialesCurso>> call(Params params) async {
    return await repository.cargarMateriales(params.codCurso);
  }
}

class Params extends Equatable {
  final String codCurso;

  Params({
    @required this.codCurso,
  });

  @override
  List<Object> get props => [codCurso];
}