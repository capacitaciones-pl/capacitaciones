import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class MaterialesCurso extends Equatable {
  final int count;
  bool enableButton;
  final List<MaterialCurso> data;

  MaterialesCurso({
    @required this.count,
    @required this.data,
  });

  @override
  List<Object> get props => [count, data];
}

class MaterialCurso extends Equatable {
  final int correlativoArchivos;
  final String nroDocReferencia;
  final String nroSubDocReferencia;
  final String codTablaRef;
  final int grupoPertenece;
  final String descripcion;
  final String tipoArchivo;
  final String nombre;
  final int size;
  final bool estado;
  final int revisado;
  final String codPersona;

  MaterialCurso({
    @required this.correlativoArchivos,
    @required this.nroDocReferencia,
    @required this.nroSubDocReferencia,
    @required this.codTablaRef,
    @required this.grupoPertenece,
    @required this.descripcion,
    @required this.tipoArchivo,
    @required this.nombre,
    @required this.size,
    @required this.estado,
    @required this.revisado,
    @required this.codPersona,
  });

  @override
  List<Object> get props => [
    correlativoArchivos,
    nroDocReferencia,
    nroSubDocReferencia,
    codTablaRef,
    grupoPertenece,
    descripcion,
    tipoArchivo,
    nombre,
    size,
    estado,
    revisado,
    codPersona,
  ];
}