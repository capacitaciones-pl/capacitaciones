import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RequestUpdate extends Equatable {
  final String codCurso;
  final String codPersona;
  final int codArchivo;

  RequestUpdate({
    @required this.codCurso,
    @required this.codPersona,
    @required this.codArchivo,
  });

  Map<String, dynamic> toJson() {
    return {
      'nroDocReferencia': codCurso,
      'codPersona': codPersona,
      'codArchivo': codArchivo,
    };
  }

  @override
  List<Object> get props => [codCurso, codPersona, codArchivo,];
}