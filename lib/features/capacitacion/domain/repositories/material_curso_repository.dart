import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:dartz/dartz.dart';

abstract class MaterialCursoRepository {
  Future<Either<Failure, MaterialesCurso>> cargarMateriales(String codCurso);

  Future<Either<Failure, MaterialesCurso>> actualizarMaterial(String codCurso, int codArchivo);
}