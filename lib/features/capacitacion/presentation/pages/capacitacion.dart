import 'dart:async';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/request_update.dart';
import 'package:app_capacitaciones/features/capacitacion/presentation/bloc/capacitacion_bloc.dart';
import 'package:app_capacitaciones/features/capacitacion/presentation/widgets/custom_material_controls.dart';
import 'package:app_capacitaciones/features/landing/domain/entities/proximas_capacitaciones.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/AppDrawer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:app_capacitaciones/injection_container.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import 'package:app_capacitaciones/core/util/input_converter.dart';

class CapacitacionPage extends StatefulWidget {
  @override
  _CapacitacionPageState createState() => _CapacitacionPageState();
}

class _CapacitacionPageState extends State<CapacitacionPage> {
  List<VideoPlayerController> _controllers = new List<VideoPlayerController>();
  List<ChewieController> _chewieControllers = new List<ChewieController>();

  @override
  void dispose() {
    _controllers.forEach((element) => element.dispose());
    _chewieControllers.forEach((element) => element.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ProximaCapacitacion curso = ModalRoute.of(context).settings.arguments;

    return BlocProvider(
      create: (context) =>
          sL<CapacitacionBloc>()..add(CargarMaterialesEvent(curso.codCurso)),
      child: Scaffold(
        appBar: ReusableWidgets.appBar("Capacitación", false),
        backgroundColor: Color.fromRGBO(234, 234, 234, 1.0),
        drawer: AppDrawer(
          index: 1,
        ),
        body: BlocBuilder<CapacitacionBloc, CapacitacionState>(
          builder: (context, state) {
            return ListView(
              padding: EdgeInsets.all(16),
              children: createItems(curso, state, context),
            );
          },
        ),
      ),
    );
  }

  List<Widget> createItems(ProximaCapacitacion curso, CapacitacionState state,
      BuildContext context) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(createHeader(curso));
    widgets.add(
      SizedBox(
        height: 16,
      ),
    );

    if (state is Loading) {
      widgets.add(
        Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    } else if (state is Loaded) {
      if (curso.intentos < 2) {
        widgets.add(
          Center(
            child: Text(
              "Solo tiene 2 intentos para rendir la evaluación.",
              style: TextStyle(
                fontSize: 14 / myTextScaleFactor,
                fontWeight: FontWeight.w400,
                color: state.materialesCurso.enableButton
                    ? Color.fromRGBO(100, 133, 16, 1.0)
                    : Color.fromRGBO(200, 200, 200, 1.0),
              ),
            ),
          ),
        );
        widgets.add(
          SizedBox(
            height: 8,
          ),
        );
        widgets
            .add(createButtonEval(curso, state.materialesCurso.enableButton));
      } else {
        widgets.add(
          Center(
            child: Text(
              "Número de intentos superado.",
              style: TextStyle(
                fontSize: 14 / myTextScaleFactor,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(100, 133, 16, 1.0),
              ),
            ),
          ),
        );
        widgets.add(
          SizedBox(
            height: 8,
          ),
        );
        widgets.add(createButtonEval(curso, false));
      }

      widgets.add(
        SizedBox(
          height: 16,
        ),
      );

      if (state.materialesCurso.count > 0) {
        widgets.add(
          Text(
            "Material del curso",
            style: TextStyle(
              fontSize: 16 / myTextScaleFactor,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(100, 133, 16, 1.0),
            ),
          ),
        );
      }

      state.materialesCurso.data.forEach((material) {
        widgets.add(
          SizedBox(
            height: 16,
          ),
        );
        if (material.tipoArchivo.contains('mp4')) {
          widgets.add(createVideoMaterial(material, context));
        } else {
          widgets.add(createPdfMaterial(material, context, curso));
        }
      });
    }

    return widgets;
  }

  Widget createPdfMaterial(MaterialCurso material, BuildContext context, ProximaCapacitacion curso) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(
              context,
              '/material',
              arguments: material,
            ).then(
                  (value) {
                BlocProvider.of<CapacitacionBloc>(context).add(CargarMaterialesEvent(curso.codCurso));
              },
            );
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.description,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                  size: 24,
                ),
                SizedBox(
                  width: 6,
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    material.nombre.toString(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 15 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
                SizedBox(
                  width: 6,
                ),
                Icon(
                  Icons.visibility,
                  color: material.revisado == 2
                      ? Color.fromRGBO(100, 133, 16, 1.0)
                      : Color.fromRGBO(210, 210, 210, 1.0),
                  size: 18,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget createVideoMaterial(MaterialCurso material, BuildContext context) {
    String codReferencia = material.nroDocReferencia;
    String codPersona = material.codPersona;
    int codArchivo = material.correlativoArchivos;
    String url =
        '$MHSEC_PLAN/accion/file/createvalfiles?nrodocreferencia=$codReferencia&codpersona=$codPersona&codarchivo=$codArchivo';

    VideoPlayerController controller;
    Duration _position;
    Duration _duration;

    controller = VideoPlayerController.network(url);

    ChewieController chewieController = ChewieController(
      videoPlayerController: controller,
      aspectRatio: 16 / 9,
      autoInitialize: true,
      showControlsOnInitialize: false,
      customControls: CustomMaterialControls(),
      //showControls: false,
    );

    controller
      ..addListener(
        () {
          _position = controller.value.position;
          _duration = controller.value.duration;

          if (_duration?.compareTo(_position) == 0 ||
              _duration?.compareTo(_position) == -1) {
            chewieController.exitFullScreen();
            controller.value = VideoPlayerValue.uninitialized();

            BlocProvider.of<CapacitacionBloc>(context).add(
              ActualizarMaterialEvent(
                codReferencia,
                codArchivo,
              ),
            );
          }
        },
      );

    _controllers.add(controller);
    _chewieControllers.add(chewieController);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(12), topLeft: Radius.circular(12)),
            child: Chewie(
              controller: chewieController,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.local_movies,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                  size: 24,
                ),
                SizedBox(
                  width: 6,
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    material.nombre.toString(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 15 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
                SizedBox(
                  width: 6,
                ),
                Icon(
                  Icons.visibility,
                  color: material.revisado == 2
                      ? Color.fromRGBO(100, 133, 16, 1.0)
                      : Color.fromRGBO(210, 210, 210, 1.0),
                  size: 18,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget createHeader(ProximaCapacitacion curso) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(
                curso.enlace != null && curso.enlace.length > 0
                    ? Icons.ondemand_video
                    : Icons.collections_bookmark,
                color: Color.fromRGBO(209, 77, 56, 1.0),
                size: 26,
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color.fromRGBO(100, 133, 16, 1.0),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            curso.tema,
            style: TextStyle(
              fontSize: 15 / myTextScaleFactor,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(85, 85, 85, 1.0),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.calendar_today,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Fecha programada",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    /*"12/07/2020 . 08:00 a.m.",*/
                    DateFormat("dd/MM/yyyy . hh:mm a").format(curso.fechaIni),
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.calendar_today,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Fecha finalización",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    /*"12/07/2020 . 08:00 a.m.",*/
                    DateFormat("dd/MM/yyyy . hh:mm a").format(curso.fechaFinal),
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.timer,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Duración",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    curso.duracion + ' hora(s)',
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.event_available,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Vigencia",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    curso.vigenciatxt,
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.assignment_ind,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Expositor",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    curso.expositores.isEmpty
                        ? 'No definido'
                        : curso.expositores[0].nombre.capitalize(),
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.business,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Emp. capacitadora",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    curso.empresaCap,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.airline_seat_recline_normal,
                color: Color.fromRGBO(85, 85, 85, 1.0),
                size: 16,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Cupos disponibles",
                style: TextStyle(
                  fontSize: 13 / myTextScaleFactor,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    curso.disponibles.toString(),
                    style: TextStyle(
                      fontSize: 13 / myTextScaleFactor,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Visibility(
            visible: curso.enlace != null && curso.enlace.length > 0,
            maintainSize: false,
            maintainState: false,
            maintainAnimation: false,
            maintainInteractivity: false,
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.link,
                  color: Color.fromRGBO(85, 85, 85, 1.0),
                  size: 16,
                ),
                SizedBox(
                  width: 6,
                ),
                Text(
                  "Link capacitación",
                  style: TextStyle(
                    fontSize: 13 / myTextScaleFactor,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(85, 85, 85, 1.0),
                  ),
                ),
                SizedBox(
                  width: 6,
                ),
                Expanded(
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () async {
                        String url = curso.enlace;
                        if (await canLaunch(url)) {
                          await launch(url);
                        }
                      },
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Abrir capacitación web",
                          style: TextStyle(
                            fontSize: 13 / myTextScaleFactor,
                            fontWeight: FontWeight.w300,
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget createButtonEval(ProximaCapacitacion curso, bool enableButton) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          if (!enableButton) return;

          Navigator.pushNamed(
            context,
            '/evaluacion',
            arguments: curso,
          );
        },
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: enableButton
                ? Color.fromRGBO(100, 133, 16, 1.0)
                : Color.fromRGBO(210, 210, 210, 1.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.5,
                blurRadius: 5,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
            child: Text(
              "Rendir Evaluación",
              style: TextStyle(
                fontSize: 17 / myTextScaleFactor,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
