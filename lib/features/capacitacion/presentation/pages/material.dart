import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:app_capacitaciones/features/capacitacion/presentation/bloc/capacitacion_bloc.dart';
import 'package:app_capacitaciones/features/capacitacion/presentation/widgets/custom_pdf_viewer.dart';
import 'package:app_capacitaciones/features/scaffold/presentation/widgets/ReusableWidgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';

class MaterialPage extends StatefulWidget {
  @override
  _MaterialPageState createState() => _MaterialPageState();
}

class _MaterialPageState extends State<MaterialPage> {
  String _url = '';
  PDFDocument _doc;
  bool _loading = true;
  bool _first = true;

  _initPdf() async {
    final doc = await PDFDocument.fromURL(_url);
    setState(() {
      _first = false;
      _doc = doc;
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    MaterialCurso curso = ModalRoute.of(context).settings.arguments;
    String codReferencia = curso.nroDocReferencia;
    String codPersona = curso.codPersona;
    int codArchivo = curso.correlativoArchivos;
    _url = '$MHSEC_PLAN/accion/file/createvalfiles?nrodocreferencia=$codReferencia&codpersona=$codPersona&codarchivo=$codArchivo';
    if (_first)
      _initPdf();

    return BlocProvider(
      create: (context) => sL<CapacitacionBloc>(),
      child: Scaffold(
        appBar: ReusableWidgets.appBar(curso.nombre, false),
        body: Builder(
          builder: (BuildContext context) {
            return _buildPdfViewer(context, codReferencia, codArchivo);
          },
        ),
      ),
    );
  }

  Widget _buildPdfViewer(BuildContext context, String codReferencia, int codArchivo) {
    return _loading ? Center(
      child: CircularProgressIndicator(),
    ) : CustomPDFViewer(
      document: _doc,
      showPicker: false,
      showIndicator: false,
      endListener: () {
        BlocProvider.of<CapacitacionBloc>(context).add(
          ActualizarMaterialEvent(
            codReferencia,
            codArchivo,
          ),
        );
      },
    );
  }
}