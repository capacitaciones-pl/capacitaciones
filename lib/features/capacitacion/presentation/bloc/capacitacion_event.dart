part of 'capacitacion_bloc.dart';

abstract class CapacitacionEvent extends Equatable {
  const CapacitacionEvent();
}

class CargarMaterialesEvent extends CapacitacionEvent {
  final String codCurso;

  CargarMaterialesEvent(this.codCurso);

  @override
  List<Object> get props => [codCurso];
}

class ActualizarMaterialEvent extends CapacitacionEvent {
  final String codCurso;
  final int codArchivo;

  ActualizarMaterialEvent(this.codCurso, this.codArchivo);

  @override
  List<Object> get props => [codCurso];
}