part of 'capacitacion_bloc.dart';

abstract class CapacitacionState extends Equatable {
  const CapacitacionState();
}

class Empty extends CapacitacionState {
  @override
  List<Object> get props => [];
}

class Loading extends CapacitacionState {
  @override
  List<Object> get props => [];
}

class Loaded extends CapacitacionState {
  final MaterialesCurso materialesCurso;

  Loaded({
    @required this.materialesCurso,
  });

  @override
  List<Object> get props => [materialesCurso];
}

class Error extends CapacitacionState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
