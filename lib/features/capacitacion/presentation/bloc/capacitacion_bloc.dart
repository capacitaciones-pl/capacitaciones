import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/entities/materiales_curso.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/usecases/actualizar_material.dart';
import 'package:app_capacitaciones/features/capacitacion/domain/usecases/cargar_material.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'capacitacion_event.dart';
part 'capacitacion_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible obtener la información.';
const String CACHE_FAILURE_MESSAGE = 'No fue posible obtener data local del usuario.';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';

class CapacitacionBloc extends Bloc<CapacitacionEvent, CapacitacionState> {
  final CargarMaterial cargarMaterial;
  final ActualizarMaterial actualizarMaterial;

  CapacitacionBloc({
    @required CargarMaterial cargarMaterialCase,
    @required ActualizarMaterial actualizarMaterialCase,
  })  : assert(cargarMaterialCase != null),
        assert(actualizarMaterialCase != null),
        cargarMaterial = cargarMaterialCase,
        actualizarMaterial = actualizarMaterialCase,
        super(Empty());

  @override
  Stream<CapacitacionState> mapEventToState(
    CapacitacionEvent event,
  ) async* {
    if (event is CargarMaterialesEvent) {
      yield Loading();

      final failureOrData =
          await cargarMaterial(Params(codCurso: event.codCurso));

      yield* failureOrData.fold(
        (failure) async* {
          yield Error(
            message: _mapFailureToMessage(failure),
          );
        },
        (data) async* {
          yield Loaded(materialesCurso: data);
        },
      );
    } else if (event is ActualizarMaterialEvent) {
      yield Loading();

      final failureOrData = await actualizarMaterial(
        ParamsA(
          codCurso: event.codCurso,
          codArchivo: event.codArchivo,
        ),
      );

      yield* failureOrData.fold(
        (failure) async* {
          yield Error(
            message: _mapFailureToMessage(failure),
          );
        },
        (data) async* {
          yield Loaded(materialesCurso: data);
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
