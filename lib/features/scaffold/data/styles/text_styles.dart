import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:flutter/material.dart';

class CustomTextStyle {
/* LoginPage */
  static var userLoginTextStyle = TextStyle(
    fontSize: 14 / myTextScaleFactor,
    color: Color.fromRGBO(209, 77, 56, 1.0),
  );

  static var passLoginTextStyle = TextStyle(
    fontSize: 14 / myTextScaleFactor,
    color: Color.fromRGBO(209, 77, 56, 1.0),
  );

  static var buttonLoginTextStyle = TextStyle(
    fontSize: 18 / myTextScaleFactor,
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );

  static var errorLoginTexStyle = TextStyle(
    fontSize: 14 / myTextScaleFactor,
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );
/* -----------*/

  static var titleTextStyle = TextStyle(
    fontSize: 17 / myTextScaleFactor,
    color: Color.fromRGBO(100, 133, 16, 1.0),
    fontWeight: FontWeight.w400,
  );

  static var userTextStyle = TextStyle(
    fontSize: 18 / myTextScaleFactor,
    color: Colors.white,
    fontWeight: FontWeight.w400,
  );

  static var drawerOptionFontSize = 16 / myTextScaleFactor;
}



