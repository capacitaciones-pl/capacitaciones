import 'package:app_capacitaciones/features/scaffold/data/models/RouteModel.dart';
import 'package:flutter/material.dart';

var myTextScaleFactor = 1.0;
var myTextSize = 12;

final Routes = [
  RouteModel(
    routeId: 0,
    icon: Icons.home,
    name: "Inicio",
    route: "/inicio",
  ),
//  RouteModel(
//    routeId: 1,
//    icon: Icons.assignment,
//    name: "Capacitaciones",
//    route: "/capacitaciones",
//  ),
  RouteModel(
    routeId: 1,
    icon: Icons.assignment_turned_in,
    name: "Mis capacitaciones",
    route: "/mis_capacitaciones",
  ),
  RouteModel(
    routeId: 2,
    icon: Icons.event_available,
    name: "Capacitaciones programadas",
    route: "/capacitaciones_programadas",
  ),
  RouteModel(
    routeId: 3,
    icon: Icons.event,
    name: "Próximas capacitaciones",
    route: "/proximas_capacitaciones",
  ),
  /*RouteModel(
    routeId: 4,
    icon: Icons.settings_applications,
    name: "Configuraciones del app",
    route: "/configuraciones",
  ),*/
  /*RouteModel(
    routeId: 4,
    icon: Icons.headset_mic,
    name: "Soporte",
    route: "/soporte",
  ),*/
  /*RouteModel(
    routeId: 6,
    icon: Icons.exit_to_app,
    name: "Salir de la aplicación",
    route: "salir",
  ),*/
];