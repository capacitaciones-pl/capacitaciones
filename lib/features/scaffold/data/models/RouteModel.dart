import 'package:flutter/material.dart';

class RouteModel {
  final int routeId;
  final String name;
  final String route;
  final IconData icon;

  RouteModel({this.routeId, this.name, this.route, this.icon});
}