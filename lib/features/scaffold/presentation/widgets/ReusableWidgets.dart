import 'package:flutter/material.dart';
import 'package:app_capacitaciones/features/scaffold/data/styles/text_styles.dart';

class ReusableWidgets {

  static Widget appBar(String title, bool showMenu) {
    return AppBar(
      elevation: 3,
      title: Text(
        title,
        style: CustomTextStyle.titleTextStyle,
      ),
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: showMenu ? Icon(Icons.sort, size: 28,) : Icon(Icons.arrow_back, size: 28,),
            onPressed: () {
              if (showMenu)
                Scaffold.of(context).openDrawer();
              else
                Navigator.pop(context);
            },
            tooltip: showMenu ? MaterialLocalizations.of(context).openAppDrawerTooltip : MaterialLocalizations.of(context).backButtonTooltip,
          );
        },
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(25),
        ),
      ),
    );
  }
}