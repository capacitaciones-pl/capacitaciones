import 'package:app_capacitaciones/features/scaffold/data/models/RouteModel.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/data/styles/text_styles.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {

  final int index;
  final String name;

  const AppDrawer({Key key, this.index, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: double.infinity,
      color: Color.fromRGBO(255, 255, 255, 1.0),
      child: Material(
        child: Column(
          children: _createItems(context),
        ),
      ),
    );
  }

  List<Widget> _createItems(BuildContext context) {
    List<Widget> items = new List<Widget>();

    items.add(
      Container(
        height: 80,
        color: Color.fromRGBO(209, 77, 56, 1.0),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            margin: EdgeInsets.fromLTRB(16, 0, 0, 16),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.person,
                  color: Colors.white,
                  size: 28,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  this.name != null ? this.name : 'Usuario',
                  style: CustomTextStyle.userTextStyle,
                ),
              ],
            ),
          ),
        ),
      ),
    );

    Routes.forEach((routeModel) => items.add(_createItem(context, routeModel)));

    items.add(
      Expanded(
        child: Align(
          alignment: Alignment.bottomRight,
          child: Container(
            margin: EdgeInsets.fromLTRB(0, 0, 16, 16),
            child: Text(
              "v1.0.0",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w300,
                color: Color.fromRGBO(85, 85, 85, 1.0),
              ),
            ),
          ),
        ),
      ),
    );

    return items;
  }

  Widget _createItem(BuildContext context, RouteModel routeModel) {
    return InkWell(
      onTap: () => {
        if (routeModel.routeId != index)
          Navigator.pushNamed(context, routeModel.route)
      },
      splashColor: Color.fromRGBO(209, 77, 56, 0.3),
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 8, 0, 8),
        child: Row(
          children: <Widget>[
            Icon(
              routeModel.icon,
              color: routeModel.routeId == index ? Color.fromRGBO(209, 77, 56, 1.0) : Color.fromRGBO(85, 85, 85, 1.0),
            ),
            SizedBox(
              width: 8,
            ),
            Text(
              routeModel.name,
              style: TextStyle(
                fontSize: CustomTextStyle.drawerOptionFontSize,
                fontWeight: FontWeight.w300,
                color: routeModel.routeId == index ? Color.fromRGBO(209, 77, 56, 1.0) : Color.fromRGBO(85, 85, 85, 1.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}