part of 'user_data_bloc.dart';

abstract class UserDataEvent extends Equatable {
  const UserDataEvent();
}

class GetUserDataEvent extends UserDataEvent {
  final String user;
  final String password;

  GetUserDataEvent({
    @required this.user,
    @required this.password,
  });

  @override
  // TODO: implement props
  List<Object> get props => [user, password];
}