import 'dart:async';

import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:app_capacitaciones/features/login/domain/usecases/get_user_data.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'user_data_event.dart';
part 'user_data_state.dart';

const String SERVER_FAILURE_MESSAGE = 'No fue posible iniciar sesión.';
const String CACHE_FAILURE_MESSAGE = 'Cache failure';
const String NO_INTERNET_FAILURE_MESSAGE = 'El dispositivo no cuenta con internet.';
const String EMPTY_FIELDS_FAILURE_MESSAGE = 'Debe ingresar usuario y contraseña.';

class UserDataBloc extends Bloc<UserDataEvent, UserDataState> {
  final GetUserData getUserData;

  UserDataBloc({
    @required GetUserData getUserDataCase,
  })  : assert(getUserDataCase != null),
        getUserData = getUserDataCase,
        super(Empty());

  @override
  Stream<UserDataState> mapEventToState(
    UserDataEvent event,
  ) async* {
    if (event is GetUserDataEvent) {
      final logonData = LogonData(
        usuario: event.user,
        password: event.password,
      );

      yield Loading();
      final failureOrUserData = await getUserData(logonData);

      yield* failureOrUserData.fold(
        (failure) async* {
          yield Error(message: _mapFailureToMessage(failure));
        },
        (userData) async* {
          yield Loaded(userData: userData);
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case NoInternetFailure:
        return NO_INTERNET_FAILURE_MESSAGE;
      case EmptyFieldsFailure:
        return EMPTY_FIELDS_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
