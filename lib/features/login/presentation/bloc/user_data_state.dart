part of 'user_data_bloc.dart';

abstract class UserDataState extends Equatable {
  const UserDataState();
}

class Empty extends UserDataState {
  @override
  List<Object> get props => [];
}

class Loading extends UserDataState {
  @override
  List<Object> get props => [];
}

class Loaded extends UserDataState {
  final UserData userData;

  Loaded({@required this.userData});

  @override
  List<Object> get props => [userData];
}

class Error extends UserDataState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
