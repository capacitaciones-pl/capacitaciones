import 'package:app_capacitaciones/features/landing/presentation/pages/landing.dart';
import 'package:app_capacitaciones/features/login/presentation/bloc/user_data_bloc.dart';
import 'package:app_capacitaciones/features/scaffold/data/routes/Routes.dart';
import 'package:app_capacitaciones/features/scaffold/data/styles/text_styles.dart';
import 'package:app_capacitaciones/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  KeyboardVisibilityNotification _keyboardVisibility = new KeyboardVisibilityNotification();
  int _keyboardVisibilitySubscriberId;
  bool _keyboardState;
  final userTextController = TextEditingController();
  final passTextController = TextEditingController();
  final userTextFocus = FocusNode();
  final passTextFocus = FocusNode();

  @protected
  void initState() {
    super.initState();

    _keyboardState = _keyboardVisibility.isKeyboardVisible;

    _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
      onChange: (bool visible) {
        setState(() {
          _keyboardState = visible;
        });
      },
    );
  }

  @override
  void dispose() {
    userTextController.dispose();
    passTextController.dispose();
    userTextFocus.dispose();
    passTextFocus.dispose();
    _keyboardVisibility.removeListener(_keyboardVisibilitySubscriberId);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    myTextScaleFactor = MediaQuery.of(context).textScaleFactor;

    return Scaffold(
      backgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
      body: buildBody(context),
    );
  }

  BlocProvider<UserDataBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (context) => sL<UserDataBloc>(),
      child: BlocListener<UserDataBloc, UserDataState>(
        listener: (context, state) {
          if (state is Loaded) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LandingPage(),
              ),
            );
          }
        },
        child: BlocBuilder<UserDataBloc, UserDataState>(
          builder: (context, state) {
            return Stack(
              children: <Widget>[
                Container(
                  color: Colors.deepOrange,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset(
                    "assets/images/background_login.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                Column(
                  children: <Widget>[
                    Visibility(
                      visible: !_keyboardState,
                      child: Expanded(
                        flex: 3,
                        child: Container(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/images/logo_anta.png",
                                    width: 150,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 12, horizontal: 0),
                                    child: Text(
                                      "¡Bienvenido!",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 30,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Capacitaciones Antapaccay",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 22,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )
                                ],
                              ),
                            )
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        margin: _keyboardState ? EdgeInsets.fromLTRB(32, 32, 32, 16) : EdgeInsets.fromLTRB(32, 16, 32, 16),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Color.fromRGBO(255, 255, 255, 0.8),
                        ),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(24, 24, 24, 12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Container(
                                  child: Text(
                                    'Usuario',
                                    style: CustomTextStyle.userLoginTextStyle,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
                                  child: TextFormField(
                                    controller: userTextController,
                                    focusNode: userTextFocus,
                                    style: CustomTextStyle.userLoginTextStyle,
                                    decoration: InputDecoration(
                                      icon: Icon(
                                        Icons.person_outline,
                                        color: Color.fromRGBO(209, 77, 56, 1.0),
                                        size: 28,
                                      ),
                                      hintText: 'Ingrese su usuario',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Color.fromRGBO(150, 150, 150, 1.0),),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Color.fromRGBO(209, 77, 56, 1.0),),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  child: Text(
                                    'Contraseña',
                                    style: CustomTextStyle.passLoginTextStyle,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
                                  child: TextField(
                                    controller: passTextController,
                                    focusNode: passTextFocus,
                                    style: CustomTextStyle.passLoginTextStyle,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      icon: Icon(
                                        Icons.lock_outline,
                                        color: Color.fromRGBO(209, 77, 56, 1.0),
                                        size: 28,
                                      ),
                                      hintText: 'Ingrese su contraseña',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Color.fromRGBO(150, 150, 150, 1.0),),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Color.fromRGBO(209, 77, 56, 1.0),),
                                      ),
                                    ),
                                    onSubmitted: (text) {
                                      userTextFocus.unfocus();
                                      passTextFocus.unfocus();
                                      BlocProvider.of<UserDataBloc>(context).add(
                                        GetUserDataEvent(
                                          user: userTextController.text.trim(),
                                          password: passTextController.text.trim(),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      //* Botón inferior -- start
                      child: Container(
                        child: Center(
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                if (state is Loading) {
                                  return;
                                }
                                userTextFocus.unfocus();
                                passTextFocus.unfocus();
                                BlocProvider.of<UserDataBloc>(context).add(
                                  GetUserDataEvent(
                                    user: userTextController.text.trim(),
                                    password: passTextController.text.trim(),
                                  ),
                                );
                              },
                              child: Container(
                                height: 35,
                                width: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(35.0),
                                  color: Color.fromRGBO(35, 100, 209, 0.9),
                                ),
                                child: state is Loading
                                    ? buildLoading()
                                    : buildText(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      //* Botón inferior -- end
                    ),
                  ],
                ),
                /*Visibility(
                  visible: state is Loading,
                  child: Container(
                    color: Colors.black.withAlpha(160),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),*/
                Visibility(
                  visible: state is Error,
                  child: Container(
                    color: Colors.black.withAlpha(160),
                    height: 24,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                Visibility(
                  visible: state is Error,
                  child: buildError(context, state),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget buildError(BuildContext context, UserDataState state) {
    if (state is Error) {
      passTextController.clear();
      return SafeArea(
        child: Container(
          color: Colors.black.withAlpha(160),
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Text(
              state.message,
              style: CustomTextStyle.errorLoginTexStyle,
            ),
          ),
        ),
      );
    }
    return Container();
  }

  Widget buildText() {
    return Center(
      child: Text(
        'Ingresar',
        style: CustomTextStyle.buttonLoginTextStyle,
      ),
    );
  }

  Widget buildLoading() {
    return Center(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40.0),
          color: Colors.white,
        ),
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 12),
        child: Image.asset('assets/images/loading.gif'),
      ),
    );
  }
}


