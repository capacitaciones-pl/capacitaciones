import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_remote_data_source.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:app_capacitaciones/features/login/domain/repositories/user_data_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class UserDataRepositoryImpl implements UserDataRepository {
  final UserDataRemoteDataSource remoteDataSource;
  final UserDataLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  UserDataRepositoryImpl(
      {@required this.remoteDataSource,
      @required this.localDataSource,
      @required this.networkInfo});

  @override
  Future<Either<Failure, UserData>> getUserData(LogonData logonData) async {
    if (logonData.usuario.isEmpty || logonData.password.isEmpty) {
      return Left(EmptyFieldsFailure());
    }

    if (await networkInfo.isConnected) {
      try {
        final remoteUserData = await remoteDataSource.getUserData(logonData);
        localDataSource.cacheUserData(remoteUserData);
        return Right(remoteUserData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoInternetFailure());
      /*try {
        final localUserData = await localDataSource.getLastUserData();
        return Right(localUserData);
      } on CacheException {
        return Left(CacheFailure());
      }*/
    }
  }
}
