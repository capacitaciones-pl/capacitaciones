import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:meta/meta.dart';

class UserDataModel extends UserData {
  UserDataModel({
    @required int codUsuario,
    @required String usuario,
    @required String token,
    @required String codPersona,
    @required int codTipoPersona,
    @required String nombres,
    @required String apellidos,
    @required String email,
    @required String empresa,
  }) : super(
          codUsuario: codUsuario,
          usuario: usuario,
          token: token,
          codPersona: codPersona,
          codTipoPersona: codTipoPersona,
          nombres: nombres,
          apellidos: apellidos,
          email: email,
          empresa: empresa,
        );

  factory UserDataModel.fromJson(Map<String, dynamic> json) {
    return UserDataModel(
        codUsuario: json['codUsuario'],
        usuario: json['usuario'],
        token: json['token'],
        codPersona: json['codPersona'],
        codTipoPersona: json['codTipoPersona'],
        nombres: json['nombres'],
        apellidos: json['apellidos'],
        email: json['email'],
        empresa: json['empresa']);
  }

  Map<String, dynamic> toJson() {
    return {
      'codUsuario': codUsuario,
      'usuario': usuario,
      'token': token,
      'codPersona': codPersona,
      'nombres': nombres,
      'apellidos': apellidos,
      'email': email,
      'empresa': empresa
    };
  }
}
