import 'dart:convert';

import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:meta/meta.dart';

abstract class UserDataLocalDataSource {

  /// Obtiene la última información de [UserDataModel] que fue almacenada
  /// en caché la última vez que se tuvo acceso a internet.
  ///
  /// Throws [CacheException] si no hay data en caché.
  Future<UserDataModel> getLastUserData();

  Future<void> cacheUserData(UserDataModel userDataModel);
}

const CACHED_USER_DATA = 'CACHED_USER_DATA';

class UserDataLocalDataSourceImpl implements UserDataLocalDataSource {
  final SharedPreferences sharedPreferences;

  UserDataLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> cacheUserData(UserDataModel userDataModel) {
    return sharedPreferences.setString(
      CACHED_USER_DATA,
      json.encode(userDataModel),
    );
  }

  @override
  Future<UserDataModel> getLastUserData() {
    final jsonString = sharedPreferences.getString(CACHED_USER_DATA);
    if (jsonString != null) {
      return Future.value(UserDataModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }
}