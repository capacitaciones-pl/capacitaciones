import 'dart:convert';

import 'package:app_capacitaciones/base_url.dart';
import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class UserDataRemoteDataSource {

  /// Calls the https://app.antapaccay.com.pe/ProPortalTest/MHSEC_Gen/api/v1/Login/Authenticate endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<UserDataModel> getUserData(LogonData logonData);
}

class UserDataRemoteDataSourceImpl implements UserDataRemoteDataSource {
  final http.Client client;

  UserDataRemoteDataSourceImpl({@required this.client});

  @override
  Future<UserDataModel> getUserData(LogonData logonData) async {
    final response = await client.post(
      '$MHSEC_GEN/login/authenticate',
      body: json.encode(logonData.toJson()),
      headers: {
        'Content-Type': 'application/json',
      },
    );

    /*await Future.delayed(
      Duration(
        seconds: 10,
      ),
    );*/

    if (response.statusCode == 200) {
      UserDataModel userDataModel = UserDataModel.fromJson(json.decode(response.body));

      if (userDataModel.codTipoPersona == 2)
        throw ServerException();

      return userDataModel;
    } else {
      throw ServerException();
    }
  }
}