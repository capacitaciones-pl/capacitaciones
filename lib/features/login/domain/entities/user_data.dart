import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class UserData extends Equatable {
  final int codUsuario;
  final String usuario;
  final String token;
  final String codPersona;
  final int codTipoPersona;
  final String nombres;
  final String apellidos;
  final String email;
  final String empresa;

  UserData({
    @required this.codUsuario,
    @required this.usuario,
    @required this.token,
    @required this.codPersona,
    @required this.codTipoPersona,
    @required this.nombres,
    @required this.apellidos,
    @required this.email,
    @required this.empresa});

  @override
  // TODO: implement props
  List<Object> get props => [codUsuario, usuario, token, codPersona, nombres,
    codTipoPersona, apellidos, email, empresa];
}