import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class LogonData extends Equatable {
  final String usuario;
  final String password;

  LogonData({
    @required this.usuario,
    @required this.password,
  });

  @override
  List<Object> get props => [usuario, password];

  Map<String, dynamic> toJson() {
    return {
      'usuario': usuario,
      'password': password,
    };
  }
}