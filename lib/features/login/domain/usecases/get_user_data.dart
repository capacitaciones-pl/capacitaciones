import 'package:app_capacitaciones/core/usescases/usecase.dart';
import 'package:app_capacitaciones/features/login/domain/repositories/user_data_repository.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:dartz/dartz.dart';

class GetUserData implements UseCase<UserData, LogonData>{
  final UserDataRepository repository;

  GetUserData(this.repository);

  @override
  Future<Either<Failure, UserData>> call(LogonData logonData) async {
    return await repository.getUserData(logonData);
  }
}
