import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:dartz/dartz.dart';

abstract class UserDataRepository {
  Future<Either<Failure, UserData>> getUserData(LogonData logonData);
}
