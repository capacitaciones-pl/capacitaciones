import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:dartz/dartz_unsafe.dart';

class InputConverter {
  Either<Failure, int> stringToUnsignedInteger(String str) {
    try {
      final integer = int.parse(str);
      if (integer < 0) throw FormatException();
      return Right(integer);
    } on FormatException {
      return Left(InvalidInputFailure());
    }
  }
}

class InvalidInputFailure extends Failure {
  @override
  List<Object> get props => [];
}

extension StringExtension on String {
  String capitalize() {
    List<String> words = this.split(' ');
    List<String> capital = List<String>();
    for(var w in words) {
      if(w.length > 0) {
        capital.add("${w[0].toUpperCase()}${w.substring(1).toLowerCase()}");
      }
    }
    return capital.join(" ");
  }
}