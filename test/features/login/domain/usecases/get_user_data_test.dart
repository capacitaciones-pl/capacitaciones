import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:app_capacitaciones/features/login/domain/repositories/user_data_repository.dart';
import 'package:app_capacitaciones/features/login/domain/usecases/get_user_data.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockUserDataRepository extends Mock implements UserDataRepository {}

void main() {
  GetUserData usecase;
  MockUserDataRepository mockUserDataRepository;

  setUp(() {
    mockUserDataRepository = MockUserDataRepository();
    usecase = GetUserData(mockUserDataRepository);
  });

  final tLogonData = LogonData(usuario: 'jacohello', password: '1234');
  final tUserData = UserData(
      codUsuario: 352,
      usuario: "jacohello",
      token: 'aabb',
      codPersona: 'S0020023686',
      nombres: 'JOSE ALEJANDRO',
      apellidos: 'COHELLO DIAZ',
      email: 'Jose.Cohello@glencore.com.pe',
      empresa: 'ANTAPACCAY'
  );

  test(
    'should get user data for logon data from the repository',
    () async {
      // arrange
      when(mockUserDataRepository.getUserData(any))
          .thenAnswer((_) async => Right(tUserData));
      // act
      final result = await usecase(tLogonData);
      // assert
      expect(result, Right(tUserData));
      verify(mockUserDataRepository.getUserData(tLogonData));
      verifyNoMoreInteractions(mockUserDataRepository);
    },
  );
}
