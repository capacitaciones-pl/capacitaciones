import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:app_capacitaciones/features/login/domain/usecases/get_user_data.dart';
import 'package:app_capacitaciones/features/login/presentation/bloc/user_data_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetUserData extends Mock implements GetUserData { }

void main() {
  UserDataBloc bloc;
  MockGetUserData mockGetUserData;

  setUp(() {
    mockGetUserData = MockGetUserData();

    bloc = UserDataBloc(
      getUserDataCase: mockGetUserData,
    );
  });

  test(
    'initialState should be Empty',
        () async {
      // assert
      expect(bloc.state, equals(Empty()));
    },
  );

  group('GetUserData', () {
    final tLogonData = LogonData(usuario: 'jacohello', password: '1234');
    final tUser = 'jacohello';
    final tPassword = '1234';
    final tUserData = UserData(
        codUsuario: 352,
        usuario: "jacohello",
        token: 'aabb',
        codPersona: 'S0020023686',
        nombres: 'JOSE ALEJANDRO',
        apellidos: 'COHELLO DIAZ',
        email: 'Jose.Cohello@glencore.com.pe',
        empresa: 'ANTAPACCAY'
    );

    test(
      'should get data from the GetUserData case',
      () async {
        // arrange
        when(mockGetUserData(any)).thenAnswer((_) async => Right(tUserData));
        // act
        bloc.add(GetUserDataEvent(
          user: tUser,
          password: tPassword,
        ));
        await untilCalled(mockGetUserData(any));
        // assert
        verify(mockGetUserData(tLogonData));
      },
    );

//    test(
//      'should emit Loaded when data is gotten successfully',
//      () async {
//        // arrange
//        when(mockGetUserData(any)).thenAnswer((_) async => Right(tUserData));
//
//        // act
//        bloc.add(GetUserDataEvent(
//          user: tUser,
//          password: tPassword,
//        ));
//        await untilCalled(mockGetUserData(any));
//        // assert
//        expect(bloc.state, equals(Loaded(userData: tUserData)));
//      },
//    );
  });
}