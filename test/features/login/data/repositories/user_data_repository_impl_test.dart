import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/core/error/failure.dart';
import 'package:app_capacitaciones/core/network/network_info.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_remote_data_source.dart';
import 'package:app_capacitaciones/features/login/data/repositories/user_data_repository_impl.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockRemoteDataSource extends Mock implements UserDataRemoteDataSource {}

class MockLocalDataSource extends Mock implements UserDataLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  UserDataRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockLocalDataSource mockLocalDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockLocalDataSource = MockLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = UserDataRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      localDataSource: mockLocalDataSource,
      networkInfo: mockNetworkInfo
    );
  });

  void runTestsOnline(Function body) {
    group('device is online', ()
    {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', ()
    {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      body();
    });
  }

  group('getUserData', () {
    final tLogonData = LogonData(usuario: 'jacohello', password: '1234');
    final tUserDataModel = UserDataModel(
        codUsuario: 352,
        usuario: "jacohello",
        token: 'aabb',
        codPersona: 'S0020023686',
        nombres: 'JOSE ALEJANDRO',
        apellidos: 'COHELLO DIAZ',
        email: 'Jose.Cohello@glencore.com.pe',
        empresa: 'ANTAPACCAY'
    );
    final UserData tUserData = tUserDataModel;

    test(
      'should check if the device is online',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        // act
        repository.getUserData(tLogonData);
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );

    runTestsOnline(() {
      test(
        'should remote data when response of remote data source is successful',
        () async {
          // arrange
          when(mockRemoteDataSource.getUserData(any))
              .thenAnswer((_) async => tUserDataModel);
          // act
          final result = await repository.getUserData(tLogonData);
          //assert
          verify(mockRemoteDataSource.getUserData(tLogonData));
          expect(result, equals(Right(tUserData)));
        },
      );

      test(
        'should cache the data locally when response of remote data source is successful',
        () async {
          // arrange
          when(mockRemoteDataSource.getUserData(any))
              .thenAnswer((_) async => tUserDataModel);
          // act
          await repository.getUserData(tLogonData);
          //assert
          verify(mockRemoteDataSource.getUserData(tLogonData));
          verify(mockLocalDataSource.cacheUserData(tUserDataModel));
        },
      );

      test(
        'should return server failure when response of remote data source is unsuccessful',
            () async {
          // arrange
          when(mockRemoteDataSource.getUserData(any))
              .thenThrow(ServerException());
          // act
          final result = await repository.getUserData(tLogonData);
          //assert
          verify(mockRemoteDataSource.getUserData(tLogonData));
          verifyZeroInteractions(mockLocalDataSource);
          expect(result, equals(Left(ServerFailure())));
        },
      );
    });

    runTestsOffline(() {
      test(
        'should return last locally data when the cached data is present',
            () async {
          // arrange
          when(mockLocalDataSource.getLastUserData())
              .thenAnswer((_) async => tUserDataModel);
          // act
          final result = await repository.getUserData(tLogonData);
          //assert
          verifyZeroInteractions(mockRemoteDataSource);
          verify(mockLocalDataSource.getLastUserData());
          expect(result, equals(Right(tUserData)));
        },
      );

      test(
        'should return CacheFailure when there is no cached data present',
            () async {
          // arrange
          when(mockLocalDataSource.getLastUserData())
              .thenThrow(CacheException());
          // act
          final result = await repository.getUserData(tLogonData);
          //assert
          verifyZeroInteractions(mockRemoteDataSource);
          verify(mockLocalDataSource.getLastUserData());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}