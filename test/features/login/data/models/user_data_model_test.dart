import 'dart:convert';

import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/login/domain/entities/user_data.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tUserDataModel = UserDataModel(
      codUsuario: 352,
      usuario: "jacohello",
      token: 'aabb',
      codPersona: 'S0020023686',
      nombres: 'JOSE ALEJANDRO',
      apellidos: 'COHELLO DIAZ',
      email: 'Jose.Cohello@glencore.com.pe',
      empresa: 'ANTAPACCAY');

  test(
    'should be a subclass of UserData entity',
    () async {
      expect(tUserDataModel, isA<UserData>());
    },
  );

  group('fromJson', () {
    test(
      'should return a valid model from the JSON',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap = json.decode(fixture('user_data.json'));
        // act
        final result = UserDataModel.fromJson(jsonMap);
        // assert
        expect(result, tUserDataModel);
      },
    );
  });

  group('toJson', () {
    test(
      'should return a JSON map containing the proper data',
        () async {
          // act
          final result = tUserDataModel.toJson();
          // assert
          final expectedMap = {
            "codUsuario": 352,
            "usuario": "jacohello",
            "token": "aabb",
            "codPersona": "S0020023686",
            "nombres": "JOSE ALEJANDRO",
            "apellidos": "COHELLO DIAZ",
            "email": "Jose.Cohello@glencore.com.pe",
            "empresa": "ANTAPACCAY",
          };
          expect(result, expectedMap);
        },
    );
  });
}
