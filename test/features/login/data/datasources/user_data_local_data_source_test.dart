import 'dart:convert';

import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_local_data_source.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences { }

void main() {
  UserDataLocalDataSourceImpl dataSource;
  MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource = UserDataLocalDataSourceImpl(
        sharedPreferences: mockSharedPreferences,
    );
  });

  group('getLastUserData', () {
    final tUserDataModel = UserDataModel.fromJson(
      json.decode(fixture('user_data_cached.json')),
    );

    test(
      'should return UserData cached from shared preferences',
      () async {
        // arrange
        when(mockSharedPreferences.getString(any))
            .thenReturn(fixture('user_data_cached.json'));
        // act
        final result = await dataSource.getLastUserData();
        // assert
        verify(mockSharedPreferences.getString(CACHED_USER_DATA));
        expect(result, equals(tUserDataModel));
      },
    );

    test(
      'should throw CacheException when there is not a cached value',
      () async {
        // arrange
        when(mockSharedPreferences.getString(any)).thenReturn(null);
        // act
        final call = dataSource.getLastUserData;
        // assert
        expect(() => call(), throwsA(isA<CacheException>()));
      },
    );
  });

  group('cacheUserData', () {
    final tUserDataModel = UserDataModel(
      codUsuario: 352,
      usuario: "jacohello",
      token: 'aabb',
      codPersona: 'S0020023686',
      nombres: 'JOSE ALEJANDRO',
      apellidos: 'COHELLO DIAZ',
      email: 'Jose.Cohello@glencore.com.pe',
      empresa: 'ANTAPACCAY',
    );

    test(
      'should call SharedPreferences to cache Data',
      () async {
        // act
        dataSource.cacheUserData(tUserDataModel);
        // assert
        final expectedJsonString = json.encode(tUserDataModel.toJson());
        verify(mockSharedPreferences.setString(
          CACHED_USER_DATA,
          expectedJsonString,
        ));
      },
    );
  });
}

