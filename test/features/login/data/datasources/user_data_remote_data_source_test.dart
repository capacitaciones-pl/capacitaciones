import 'dart:convert';

import 'package:app_capacitaciones/core/error/exceptions.dart';
import 'package:app_capacitaciones/features/login/data/datasources/user_data_remote_data_source.dart';
import 'package:app_capacitaciones/features/login/data/models/user_data_model.dart';
import 'package:app_capacitaciones/features/login/domain/entities/logon_data.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  UserDataRemoteDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = UserDataRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.post(
      any,
      body: anyNamed('body'),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => http.Response(fixture('user_data.json'), 200));
  };

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.post(
      any,
      body: anyNamed('body'),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => http.Response('error', 404));
  }

  group('getUserData', () {
    final tLogonData = LogonData(usuario: 'jacohello', password: '1234');
    final tUserDataModel =
        UserDataModel.fromJson(json.decode(fixture('user_data.json')));

    test(
      'should perform a POST request on a URL with the LogonData request',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource.getUserData(tLogonData);
        // assert
        verify(mockHttpClient.post(
          'https://app.antapaccay.com.pe/proportaltest/mhsec_gen/api/v1/login/authenticate',
          body: json.encode(tLogonData.toJson()),
          headers: {
            'Content-Type': 'application/json',
          },
        ));
      },
    );

    test(
      'should return UserData when the response code is 200(success)',
          () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await dataSource.getUserData(tLogonData);
        // assert
        expect(result, equals(tUserDataModel));
      },
    );

    test(
      'should throw a ServerException when the response code is 404 or other',
          () async {
        // arrange
        setUpMockHttpClientFailure404();
        // act
        final call = dataSource.getUserData;
        // assert
        expect(() => call(tLogonData), throwsA(isA<ServerException>()));
      },
    );
  });
}
